/*
	This file is part of NamCap.

	NamCap is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	NamCap is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with NamCap.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef GAME_PLAYER_H
#define GAME_PLAYER_H

extern void          player_init(unsigned char f_lin, unsigned char f_col, unsigned char f_tile);
extern unsigned char player_check_input(void);
extern unsigned char player_check_map(unsigned int f_index);
extern unsigned char player_move(void);
extern void          player_turn(void);
extern unsigned char player_move_walk(void);
extern void          player_score_add(unsigned int f_score) __z88dk_fastcall;
extern void          player_pick_item(unsigned char l_val) __z88dk_fastcall;
extern void          player_lost_life(void);
extern void          player_pick(void);
extern unsigned char player_check_up();
extern unsigned char player_check_down();
extern unsigned char player_check_left();
extern unsigned char player_check_right();


#endif
