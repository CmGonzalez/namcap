/*
        This file is part of NamCap.

        NamCap is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        NamCap is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with NamCap.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "game.h"
#include "game_audio.h"
#include "game_banks.h"
#include "game_enemies.h"
#include "game_engine.h"
#include "game_player.h"
#include "game_sprite.h"
#include "game_zx.h"
#include "macros.h"

#include <arch/zx.h>
#include <arch/zx/nirvana+.h>
#include <input.h>
#include <stdlib.h>

void player_turn(void) {

  sprite = INDEX_P1;
  if (spr_chktime()) {
    zx_border(INK_BLACK); // TODO By removing this disable 48 sound?
    s_lin0 = lin[INDEX_P1];
    s_col0 = col[INDEX_P1];
    s_colint0 = colint[INDEX_P1];

    index0 = s_col0 + ((s_lin0 >> 3) << 5);
    player_move();
    player_collision();
    if ((s_lin0 & 7) == 0) {
      player_pick_item(0);
      player_pick_item(1);
      player_pick_item(32);
      player_pick_item(33);
      if (game_object_count == 0) {
        game_round_up = 1;
      }
    }

// Magic Keys
#ifdef DEBUG
// player_debug_keys();
/*
zx_print_str(23, 8, "c");
zx_print_chr(23, 9, col[INDEX_P1]);

zx_print_str(23, 12, "i");
zx_print_chr(23, 13, colint[INDEX_P1]);

zx_print_str(23, 16, "l");
zx_print_chr(23, 17, lin[INDEX_P1]);
*/
//zx_print_chr(21, 7, colint[INDEX_P1]);
//zx_print_str(22, 0, "LIN");
//zx_print_chr(22, 3, lin[INDEX_P1]);
//zx_print_chr(22, 7, (lin[INDEX_P1] % 8));

// zx_print_chr(21,10,BIT_CHK(state[INDEX_P1], STAT_DIRU));
// zx_print_chr(21,14,BIT_CHK(state[INDEX_P1], STAT_DIRD));
// zx_print_chr(22,10,BIT_CHK(state[INDEX_P1], STAT_DIRR));
// zx_print_chr(22,14,BIT_CHK(state[INDEX_P1], STAT_DIRL));
#endif
  }
}

void player_init(unsigned char f_lin, unsigned char f_col,
                 unsigned char f_tile) {
  // COMMON SPRITE VARIABLES
  spr_kind[INDEX_P1] = E_PLAYER;
  spr_speed[INDEX_P1] = PLAYER_SPEED;
  spr_frames[INDEX_P1] = 4;
  spr_lin_inc[INDEX_P1] = 2;

  lin[INDEX_P1] = f_lin; //*SPRITELIN(INDEX_P1);
  col[INDEX_P1] = f_col; //*SPRITECOL(INDEX_P1);
  player_jump_top = f_lin;
  state[INDEX_P1] = 0;
  direction[INDEX_P1] = DIR_RIGHT;
  last_time[INDEX_P1] = zx_clock();
  player_vel_y = 0;

  // PLAYER ONLY VARIABLES

  if (value_a[INDEX_P1]) {
    colint[INDEX_P1] = 3;
    direction[INDEX_P1] = DIR_LEFT;
    tile[INDEX_P1] = f_tile + 4;
    colint[INDEX_P1] = value_b[INDEX_P1];
  } else {
    direction[INDEX_P1] = DIR_RIGHT;
    tile[INDEX_P1] = f_tile;
    colint[INDEX_P1] = value_b[INDEX_P1];
  }
  value_d[INDEX_P1] = lin[INDEX_P1];
  value_e[INDEX_P1] = col[INDEX_P1];
  // NIRVANAP_spriteT(NIRV_SPRITE_P1, tile[INDEX_P1] + colint[INDEX_P1],
  //                 lin[INDEX_P1], col[INDEX_P1]);
}

unsigned char player_move(void) {

  if (!(lin[sprite] & 7) && (col[sprite] > 0)) {
    // Col > 0 to avoid crashes when changing dir on the edges
    switch (game_atrac) {
    case 0:
      player_read_input();
      break;
    case 2:
      player_rand_input();
      break;
    }
    if (game_atrac) {

    } else {
      player_read_input();
    }
  }

  switch (direction[INDEX_P1]) {
  case DIR_RIGHT:
    if (player_check_right()) {
      spr_move_right();
    }

    tile[INDEX_P1] = TILE_P1_RIGHT;
    break;
  case DIR_LEFT:
    if (player_check_left()) {
      spr_move_left();
    }

    tile[INDEX_P1] = TILE_P1_RIGHT + 4;
    break;
  case DIR_UP:
    if (player_check_up()) {
      spr_move_up();
    }

    tile[INDEX_P1] = TILE_P1_RIGHT + 8;
    break;
  case DIR_DOWN:
    if (player_check_down()) {
      spr_move_down();
    }

    tile[INDEX_P1] = TILE_P1_RIGHT + 12;
    break;
  }

  NIRVANAP_spriteT(NIRV_SPRITE_P1, tile[INDEX_P1] + colint[INDEX_P1],
                   lin[INDEX_P1], col[INDEX_P1]);

  spr_back_repaint_dir();
  return 0;
}

void player_rand_brain_h() {
  v0 = rand() & 1;
  /* code */
  if (v0 && player_check_right()) {
    direction[INDEX_P1] = DIR_RIGHT;
  } else {
    if (player_check_left()) {
      direction[INDEX_P1] = DIR_LEFT;
    }
  }
  if (v1 && player_check_left()) {
    direction[INDEX_P1] = DIR_LEFT;
  } else {
    if (player_check_right()) {
      direction[INDEX_P1] = DIR_RIGHT;
    }
  }
}

void player_rand_brain_v() {
  v0 = rand() & 1;
  /* code */
  if (v1 && player_check_up()) {
    direction[INDEX_P1] = DIR_UP;
  } else {
    if (player_check_down()) {
      direction[INDEX_P1] = DIR_DOWN;
    }
  }
  if (v0 && player_check_down()) {
    direction[INDEX_P1] = DIR_DOWN;
  } else {
    if (player_check_up()) {
      direction[INDEX_P1] = DIR_UP;
    }
  }
}
void player_rand_input() {

  v0 = rand();
  if (v0 < 8) {
    if (direction[INDEX_P1] != DIR_LEFT && player_check_right())
      direction[INDEX_P1] = DIR_RIGHT;
  } else {
    if (v0 < 16) {
      if (direction[INDEX_P1] != DIR_DOWN && player_check_up())
        direction[INDEX_P1] = DIR_UP;
    } else {
      if (v0 < 24) {
        if (direction[INDEX_P1] != DIR_RIGHT && player_check_left())
          direction[INDEX_P1] = DIR_LEFT;
      } else {
        if (v0 < 32) {
          if (direction[INDEX_P1] != DIR_UP && player_check_down())
            direction[INDEX_P1] = DIR_DOWN;
        }
      }
    }
  }

  switch (direction[INDEX_P1]) {
  case DIR_UP:
    if (!player_check_up()) {

      direction[INDEX_P1] = DIR_DOWN;
      player_rand_brain_h();
    }
    break;
  case DIR_DOWN:
    if (!player_check_down()) {
      direction[INDEX_P1] = DIR_UP;
      player_rand_brain_h();
    }
    break;
  case DIR_RIGHT:
    if (!player_check_right()) {
      direction[INDEX_P1] = DIR_LEFT;
      player_rand_brain_v();
    }
    break;
  case DIR_LEFT:
    if (!player_check_left()) {
      direction[INDEX_P1] = DIR_RIGHT;
      player_rand_brain_v();
    }
    break;
  }
}

unsigned char player_read_input() {
  dirs = (joyfunc1)(&k1);

if ((dirs & IN_STICK_FIRE)) {
  game_round_up = 1;
}
  /* Move Right */

  if ((dirs & IN_STICK_RIGHT) && player_check_right()) {

    if (direction[INDEX_P1] == DIR_UP || direction[INDEX_P1] == DIR_DOWN) {
      colint[INDEX_P1] = 0;
    }
    spr_set_right();
    return 1;
  }

  /* Move Left */
  if ((dirs & IN_STICK_LEFT) && player_check_left()) {

    if (direction[INDEX_P1] == DIR_UP || direction[INDEX_P1] == DIR_DOWN) {
      colint[INDEX_P1] = spr_frames[sprite];
    }
    spr_set_left();
    return 1;
  }

  /* Move Up */
  if (dirs & IN_STICK_UP && player_check_up()) {
    spr_set_up();
    return 1;
  }

  /* Move Down */
  if (dirs & IN_STICK_DOWN && player_check_down()) {
    spr_set_down();
    return 1;
  }
  return 0;
}

unsigned char player_check_up() {
  // index1 = spr_calc_index(s_lin0 - SPRITE_LIN_INC, s_col0);
  index1 = s_col0 + (((s_lin0 - spr_lin_inc[sprite]) >> 3) << 5);
  return player_check_map(index1) && player_check_map(index1 + 1);
}

unsigned char player_check_down() {
  // index1 = spr_calc_index(s_lin0 + (16), s_col0);

  if (s_lin0 < LIN_FLOOR) {
    index1 = s_col0 + (((s_lin0 + 16) >> 3) << 5);
    return player_check_map(index1) && player_check_map(index1 + 1);
  } else {
    return 0;
  }
}

unsigned char player_check_left() {
  if (s_col0 > 0) {
    // index1 = spr_calc_index(s_lin0, s_col0 - 1);
    // index1 = (s_col0 - 1) + (((s_lin0) >> 3) << 5);
    index1 = index0 - 1;
    return player_check_map(index1) && player_check_map(index1 + 32);
  } else {
    return 1;
  }
}

unsigned char player_check_right() {
  if (s_col0 < 30) {
    // index1 = spr_calc_index(s_lin0, s_col0 + 2);
    // index1 = (s_col0 + 2) + (((s_lin0) >> 3) << 5);
    index1 = index0 + 2;
    return player_check_map(index1) && player_check_map(index1 + 32);
  } else {
    return 1;
  }
}

unsigned char player_check_map(unsigned int f_index) {
  if (tile_class[scr_map[f_index]] != TILE_WALL) {
    return 1;
  } else {
    return 0;
  }
}

void player_collision() {
  // Left
  unsigned char f_sprite;

  // Sprite Collision
  f_sprite = 0;
  while (f_sprite < spr_count) {
    switch (spr_kind[f_sprite]) {
    case E_GHOST:
      if ((abs(col[f_sprite] - col[INDEX_P1]) <= 1) &&
          (abs(lin[f_sprite] - lin[INDEX_P1]) < 16))
        player_kill();
      break;
    case E_WIMP:
      if ((abs(col[f_sprite] - col[INDEX_P1]) <= 1) &&
          (abs(lin[f_sprite] - lin[INDEX_P1]) < 16))
        enemy_kill(f_sprite);
      break;
    case E_WIMP_ALT:
      if ((abs(col[f_sprite] - col[INDEX_P1]) <= 1) &&
          (abs(lin[f_sprite] - lin[INDEX_P1]) < 16))
        enemy_kill(f_sprite);
      break;
    case E_FRUIT:

      if ((abs(col[f_sprite] - col[INDEX_P1]) <= 1) &&
          (abs(lin[f_sprite] - lin[INDEX_P1]) < 16)) {
        player_fruit();
      }

      break;
    }
    ++f_sprite;
  }
}

void player_kill() {
  player_kill_index = 0xFFFF;
  BIT_SET(state[INDEX_P1], STAT_KILLED);
}

unsigned char player_check_input(void) {

  if ((dirs & IN_STICK_LEFT) && (dirs & IN_STICK_RIGHT)) {
    // Error on reading both horizontal's
    dirs = 0;
  }
  return 0;
}

void player_score_add(unsigned int f_score) __z88dk_fastcall {
  if (!game_atrac) {
    player_score = player_score + f_score;

    // CHECK FOR TOP SCORE
    if (player_score > game_score_top) {
      game_score_top = player_score;
    }

    // CHECK FOR EXTRA life
    if (player_score > player_next_extra) {
      if (player_lives < 255) {
        player_lives = player_lives + 1;
      }

      player_next_extra = player_next_extra + 10000;
    }
    // ROTATE HIGH SCORE
    if (player_score > 1000000) {
      player_score = 0;
      player_next_extra = 10000;
    }

    game_print_score();
  }
}

void player_pick_item(unsigned char l_val) __z88dk_fastcall {
  unsigned char f_tile = scr_map[index0 + l_val];

  switch (tile_class[f_tile]) {
  case TILE_DOT:
    scr_map[index0 + l_val] = TILE_EMPTY;
    audio_eat_dot();
    player_score_add(1);
    if (game_object_count)
      --game_object_count;
    break;
  case TILE_PILL:
    scr_map[index0 + l_val] = TILE_EMPTY;
    scr_map[index0 + l_val + 1] = TILE_EMPTY;
    audio_eat_pill();
    player_eat_count = 0;
    player_score_add(5);
    i = 0;
    while (i < 4) {
      last_time_a[i] = zx_clock();

      if ((spr_kind[i] == E_GHOST) || (spr_kind[i] == E_WIMP_ALT)) {
        value_a[i] = 56;
        spr_kind[i] = E_WIMP;
        /* Ghost run away from Nam-Cap */
        if (direction[i] == DIR_RIGHT || direction[i] == DIR_LEFT) {
          if (col[i] > col[INDEX_P1]) {
            direction[i] = DIR_RIGHT;
          } else {
            direction[i] = DIR_LEFT;
          }
        } else {
          if (lin[i] > lin[INDEX_P1]) {
            direction[i] = DIR_UP;
          } else {
            direction[i] = DIR_DOWN;
          }
        }
      }
      ++i;
    }

    break;
  }
}

void player_lost_life() {

  --player_lives;
  game_sprite_restart(0);
  game_sprite_restart(1);
  game_sprite_restart(2);
  game_sprite_restart(3);
  NIRVANAP_spriteT(NIRV_SPRITE_FRUIT, 0, 0, 0);
  NIRVANAP_halt();
  NIRVANAP_fillT(PAPER_BLACK | INK_BLACK, lin[INDEX_FRUIT], col[INDEX_FRUIT]);

  i = 0;
  while (i < 10) {
    NIRVANAP_spriteT(NIRV_SPRITE_P1, 80 + i, lin[INDEX_P1], col[INDEX_P1]);
    ++i;
    if ((i & 1) == 0) {
      // audio_eat_gua();
      // audio_eat_pill();
      if (!game_atrac) audio_fx(7);
      // audio_beep_fx(2);

    } else {
      z80_delay_ms(5);
    }
    // audio_dead();
    // z80_delay_ms(25);
  }
  // audio_fx2(128);
  audio_beep_fx(3);
  z80_delay_ms(25);
  audio_beep_fx(3);
  // audio_fx2(160);

  NIRVANAP_fillT(PAPER_BLACK | INK_BLACK, lin[INDEX_P1], col[INDEX_P1]);

  NIRVANAP_spriteT(NIRV_SPRITE_P1, 0, 0, 0);

  direction[INDEX_P1] = DIR_RIGHT;
  lin[INDEX_P1] = value_d[INDEX_P1];
  col[INDEX_P1] = value_e[INDEX_P1];
  /* Fruit Clear */
  NIRVANAP_spriteT(NIRV_SPRITE_FRUIT, 0, 0, 0);
  spr_kind[INDEX_FRUIT] = E_NONE;
  last_time[INDEX_FRUIT] = zx_clock();

  z80_delay_ms(100);

  BIT_CLR(state[INDEX_P1], STAT_KILLED);
  loop_count = 0;
  game_print_footer();
  if (player_lives == 0) {
    game_over = 1;
  }
}

void player_fruit() {
  unsigned int f_score;

  NIRVANAP_spriteT(NIRV_SPRITE_FRUIT, 0, 0, 0);
  NIRVANAP_fillT(INK_BLACK | PAPER_BLACK, lin[INDEX_FRUIT], col[INDEX_FRUIT]);

  if (!game_atrac) audio_beep_fx(1);
  game_scr_val();
  f_score = scr_score[v0];
  player_score_add(f_score);
  spr_kind[INDEX_FRUIT] = E_NONE;

  last_time[INDEX_FRUIT] = zx_clock();
  --spr_count;
}
