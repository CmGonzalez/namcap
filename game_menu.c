/*
        This file is part of NamCap.

        NamCap is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        NamCap is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with NamCap.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "game.h"
#include "game_audio.h"
#include "game_banks.h"
#include "game_enemies.h"
#include "game_engine.h"
#include "game_player.h"
#include "game_sprite.h"
#include "game_zx.h"
#include "macros.h"
#include <arch/zx.h>
#include <arch/zx/nirvana+.h>
#include <intrinsic.h>
#include <stdlib.h>
#include <string.h>
#include <z80.h>

void menu_main() {

  unsigned char f_input;
  unsigned char s_col;
  unsigned char s_col_e;
  unsigned char s_row;
  unsigned char c;
  unsigned int start_time;

  f_input = 1;
  s_col = 12;
  s_col_e = 12 + 9;

  c = 0;
  game_code = 0;
  map_paper = PAPER_BLACK;

  game_cls();
  menu_logo();
  audio_menu();
  game_attribs();

  menu_main_print();
  i = 0;
  start_time = zx_clock();
  color = 0;
  while (f_input) {
    z80_delay_ms(20);
    // in_wait_key();
    c = in_inkey();
    // in_wait_nokey();

    s_row = ((9 + menu_curr_sel) << 3) + 8;
    // ROTATE PAPER
    // v0 = attrib_1[0];
    v0 = color << 3;
    ++color;
    if (color == INK_CYAN) {
      ++color;
    }
    if (color > 6) {
      color = 0;
    }

    attrib_1[0] = (attrib_1[0] & 0xC7) | v0;
    attrib_1[1] = (attrib_1[1] & 0xC7) | v0;
    attrib_1[2] = (attrib_1[2] & 0xC7) | v0;
    attrib_1[3] = (attrib_1[3] & 0xC7) | v0;
    game_paint_attrib(&attrib_1, s_col + 1, s_col_e, s_row);
    // 48
    c = c - 48;
    switch (c) {
    case 1: // SINCLAIR
      joyfunc1 = (uint16_t(*)(udk_t *))(in_stick_sinclair1);
      menu_curr_sel = 1;
      break;
    case 2: // KEYBOARD
      joyfunc1 = (uint16_t(*)(udk_t *))(in_stick_keyboard);
      menu_curr_sel = 2;
      break;
    case 3: // KEMPSTON
      joyfunc1 = (uint16_t(*)(udk_t *))(in_stick_kempston);
      menu_curr_sel = 3;
      break;
    case 4: // DEFINE
      menu_redefine();
      joyfunc1 = (uint16_t(*)(udk_t *))(in_stick_keyboard);
      menu_curr_sel = 2;
      start_time = zx_clock();
      break;

    case 0:
      audio_eat_pill();
      game_cls();
      ay_reset();
      // audio_game_start();
      game_tune = 0; // default
      f_input = 0;   // Exit Loop
      break;
    }
    if (c > 0 && c < 5)
      game_paint_attrib(&attrib, s_col, s_col_e, s_row);
    ++i;

    if (game_check_time(&start_time, 1000)) {
      game_atrac = 1;
      game_atrac_seq();
      start_time = zx_clock();
      zx_border(INK_BLACK);
      game_attribs();
      game_cls();
      menu_logo();
      menu_clear();
      menu_main_print();
      audio_menu();
      game_atrac = 0;
    }
  }
}

void menu_main_print(void) {

  // intrinsic_halt();
  menu_clear();
  zx_print_str(10, 11, "1 SINCLAIR");
  zx_print_str(11, 11, "2 KEYBOARD");
  zx_print_str(12, 11, "3 KEMPSTON");
  zx_print_str(13, 11, "4 DEFINE");

  game_attrib(INK_MAGENTA);
  game_paint_attrib(&attrib, 0, 31, (16 << 3) + 8);
  zx_print_str(16, 11, "0 START");
  game_attrib(INK_YELLOW);
  zx_print_str(21, 8, "2019  NOENTIENDO");
  zx_print_ink(INK_BLUE);
  zx_print_str(23, 4, "ALFA2 - DON'T DISTRIBUTE!");
}

void menu_clear() {
  zx_print_ink(PAPER_BLACK | INK_WHITE | BRIGHT);
  for (v0 = 8; v0 < 23; ++v0) {
    game_fill_row(v0, 32);
    game_paint_attrib(&attrib, 0, 31, (v0 << 3) + 8);
  }
}

void menu_redefine() {

  menu_clear();
  s_row1 = 11;
  zx_print_str(s_row1, 10, "PRESS A KEY");
  game_paint_attrib(&attrib, 0, 31, (s_row1 << 3) + 8);
  s_row1++;
  s_row1++;
  zx_print_str(s_row1, 12, "LEFT");
  k1.left = menu_define_key();
  s_row1++;
  zx_print_str(s_row1, 12, "RIGHT");
  k1.right = menu_define_key();
  s_row1++;
  zx_print_str(s_row1, 12, "UP");
  k1.up = menu_define_key();
  s_row1++;
  zx_print_str(s_row1, 12, "DOWN");
  k1.down = menu_define_key();

  // k1.up = IN_KEY_SCANCODE_DISABLE;
  // k1.down = IN_KEY_SCANCODE_DISABLE;
  z80_delay_ms(255);
  // game_fill_row(12, 32);
  menu_main_print();
}

unsigned int menu_define_key() {

  while (1) {
    in_wait_key();
    v0 = in_inkey();
    v1 = v0;
    in_wait_nokey();
    if (v1 >= 61 && v1 <= 122) {
      v1 = v1 - 32; // TO UPPER
    }
    if ((v1 >= 30 && v1 <= 39) || (v1 >= 65 && v1 <= 90)) {
      zx_print_char(s_row1, 18, v1);
    }
    if (v1 == 13) {
      zx_print_str(s_row1, 18, "ENTER");
    }
    if (v1 == 32) {
      zx_print_str(s_row1, 18, "SPACE");
    }
    return in_key_scancode(v0);
  }
}

unsigned char menu_read_key(unsigned char row, unsigned char col) {
  unsigned char key;

  while (1) {
    in_wait_key();
    key = in_inkey();
    in_wait_nokey();
    // if (key >= 48 && key <=57) {
    // if (key >= 65 && key <= 90) {
    if (key >= 97 && key <= 122) {
      key = key - 32; // UPPERCASE
      zx_print_char(row, col, key);
      return key;
    }
  }
}

void menu_logo() {
  game_cls();

  v0 = 0;
  s_col1 = 0;
  s_lin1 = 24;
  while (v0 < 16) {
    NIRVANAP_drawT(104 + v0, s_lin1, s_col1 + 8);
    ++v0;
    s_col1 = s_col1 + 2;
    if (v0 == 8) {
      s_lin1 = s_lin1 + 16;
      s_col1 = 0;
    }
  }
}

void menu_rotcolor() {
  ++color;
  if (color > 7) {
    color = 0;
  }
}

unsigned char game_atrac_seq() {
  unsigned int start_time;
  unsigned char c;
  unsigned char f_delay;

  f_delay = 120;

  ay_reset();
  game_cls();
  menu_clear();
  game_sprite_reset();

  game_attrib(INK_WHITE);
  game_paint_attrib(&attrib, 0, 31, (1 << 3) + 8);
  zx_print_str(1, 6, "CHARACTER / NICKNAME");

  c = in_inkey();
  if (!c) {
    game_attrib(INK_RED);
    game_paint_attrib(&attrib, 0, 31, (4 << 3) + 8);
    z80_delay_ms(f_delay);
    NIRVANAP_drawT(31, 42, 3);
    z80_delay_ms(f_delay);
    zx_print_str(4, 7, "EKAKIO... ");
    z80_delay_ms(f_delay);
    zx_print_str(4, 18, "'FELIPEI'");
  }

  c = in_inkey();
  if (!c) {
    game_attrib(INK_MAGENTA);
    game_paint_attrib(&attrib, 0, 31, (7 << 3) + 8);
    z80_delay_ms(f_delay);
    NIRVANAP_drawT(39, 66, 3);
    z80_delay_ms(f_delay);
    zx_print_str(7, 7, "ESUBIHCAM...");
    z80_delay_ms(f_delay);
    zx_print_str(7, 20, "'ALEJY'");
  }
  c = in_inkey();
  if (!c) {
    game_attrib(INK_CYAN);
    game_paint_attrib(&attrib, 0, 31, (10 << 3) + 8);
    z80_delay_ms(f_delay);
    NIRVANAP_drawT(47, 90, 3);
    z80_delay_ms(f_delay);
    zx_print_str(10, 7, "ERUGAMIK...");
    z80_delay_ms(f_delay);
    zx_print_str(10, 18, "'ERRAZKE'");
  }
  c = in_inkey();
  if (!c) {
    game_attrib(INK_YELLOW);
    game_paint_attrib(&attrib, 0, 31, (13 << 3) + 8);
    z80_delay_ms(f_delay);
    NIRVANAP_drawT(55, 114, 3);
    z80_delay_ms(f_delay);
    zx_print_str(13, 7, "EKOBOTO...");
    z80_delay_ms(f_delay);
    zx_print_str(13, 18, "'BEYKITO'");
  }
  c = in_inkey();
  if (!c) {
    game_attrib(INK_WHITE);
    game_paint_attrib(&attrib, 0, 31, (19 << 3) + 8);
    NIRVANAP_printQ(spr8_lookup_s[2], spr8_lookup_e[2], (19 << 3) + 8, 10);
    zx_print_str(19, 12, "10 PTS");

    z80_delay_ms(f_delay);
    game_paint_attrib(&attrib, 0, 31, (20 << 3) + 8);
    NIRVANAP_printQ(spr8_lookup_s[30], spr8_lookup_e[30], (20 << 3) + 8, 10);
    NIRVANAP_printQ(spr8_lookup_s[31], spr8_lookup_e[31], (20 << 3) + 8, 11);
    zx_print_str(20, 12, "50 PTS");

    s_lin1 = 136;
    NIRVANAP_printQ(spr8_lookup_s[30], spr8_lookup_e[30], s_lin1 + 4, 1);
    NIRVANAP_printQ(spr8_lookup_s[31], spr8_lookup_e[31], s_lin1 + 4, 2);

    player_init(s_lin1, 30, TILE_P1_RIGHT);
    col[INDEX_P1] = 28;
    direction[INDEX_P1] = DIR_LEFT;
    player_eat_count = 0;
  }
  while (col[INDEX_P1] < 30 && !c) {
    player_turn();
    enemy_turn();
    c = in_inkey();
    switch (col[INDEX_P1]) {
    case 1:
      direction[INDEX_P1] = DIR_RIGHT;
      direction[0] = DIR_RIGHT;
      direction[1] = DIR_RIGHT;
      direction[2] = DIR_RIGHT;
      direction[3] = DIR_RIGHT;
      value_a[0] = 64;
      value_a[1] = 64;
      value_a[2] = 64;
      value_a[3] = 64;
      spr_speed[0] = 4;
      spr_speed[1] = 4;
      spr_speed[2] = 4;
      spr_speed[3] = 4;
      colint[3] = 2; // Fine Tune
      break;
    case 20:
      if (direction[INDEX_P1] == DIR_LEFT && colint[INDEX_P1] == 0) {
        if (spr_kind[0] == E_NONE) {
          enemy_init(64);
          spr_kind[0] = E_HORIZONTAL;
          lin[0] = s_lin1;
          col[0] = 28;
          direction[0] = DIR_LEFT;
          spr_speed[0] = 2;
        }
      }
      break;
    case 18:
      if (direction[INDEX_P1] == DIR_LEFT && colint[INDEX_P1] == 0) {
        if (spr_kind[1] == E_NONE) {
          enemy_init(66);
          spr_kind[1] = E_HORIZONTAL;
          lin[1] = s_lin1;
          col[1] = 28;
          direction[1] = DIR_LEFT;
          spr_speed[1] = 2;
        }
      }
      break;
    case 16:
      if (direction[INDEX_P1] == DIR_LEFT && colint[INDEX_P1] == 0) {
        if (spr_kind[2] == E_NONE) {
          enemy_init(68);
          spr_kind[2] = E_HORIZONTAL;
          lin[2] = s_lin1;
          col[2] = 28;
          direction[2] = DIR_LEFT;
          spr_speed[2] = 2;
        }
      }
      break;
    case 14:

      if (direction[INDEX_P1] == DIR_LEFT && colint[INDEX_P1] == 0) {
        if (spr_kind[3] == E_NONE) {
          enemy_init(70);
          spr_kind[3] = E_HORIZONTAL;
          lin[3] = s_lin1;
          col[3] = 28;
          direction[3] = DIR_LEFT;
          spr_speed[3] = 2;
        }
      }
      break;
    case 15:
      if (direction[INDEX_P1] == DIR_RIGHT && colint[INDEX_P1] == 3) {
        NIRVANAP_spriteT(0, 0, 0, 0);
        NIRVANAP_fillT(INK_BLACK | PAPER_BLACK, s_lin1, col[0]);
        enemy_kill(0);
        spr_kind[0] = E_NONE;
      }
      break;
    case 17:
      if (direction[INDEX_P1] == DIR_RIGHT && colint[INDEX_P1] == 3) {
        NIRVANAP_spriteT(1, 0, 0, 0);
        NIRVANAP_fillT(INK_BLACK | PAPER_BLACK, s_lin1, col[1]);
        enemy_kill(1);
        spr_kind[1] = E_NONE;
      }
      break;
    case 21:
      if (direction[INDEX_P1] == DIR_RIGHT && colint[INDEX_P1] == 3) {
        NIRVANAP_spriteT(2, 0, 0, 0);
        NIRVANAP_fillT(INK_BLACK | PAPER_BLACK, s_lin1, col[2]);
        enemy_kill(2);
        spr_kind[2] = E_NONE;
      }
      break;
    case 23:
      if (direction[INDEX_P1] == DIR_RIGHT && colint[INDEX_P1] == 3) {
        NIRVANAP_spriteT(3, 0, 0, 0);
        NIRVANAP_fillT(INK_BLACK | PAPER_BLACK, s_lin1, col[3]);
        enemy_kill(3);
        spr_kind[3] = E_NONE;
        col[INDEX_P1] = 0xFF; // Exit Loop
      }

      break;
    }
  }
  menu_playwithmyself();

  game_sprite_reset();
  start_time = zx_clock();
  return 0;
}

void menu_playwithmyself() {
  unsigned int play_time;

  game_sprite_reset();
  player_init(s_lin1, 30, TILE_P1_RIGHT);
  scr_curr = 0;
  game_atrac = 2;
  game_round_init();
  play_time = zx_clock();
  col[INDEX_P1] = 15;
  lin[INDEX_P1] = 152;
  while (!game_check_time(&play_time, 2500) && !in_inkey()) {


    player_turn();
    enemy_turn();
    if (game_check_time(&time_key, 4)) {
      time_key = zx_clock();

      zx_print_ink((map_border << 3) | color);
      zx_print_str(23, 11, " NAM  CAP ");
      menu_rotcolor();
    }
  }
}
