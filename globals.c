/*
        This file is part of NamCap.

        NamCap is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        NamCap is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with NamCap.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "game.h"
#include "globals.h"
#include <input.h>
#include "globals_06_map.h"

unsigned char spec128;

// General Use VARIABLES
unsigned char i;
unsigned char v0;
unsigned char v1;
unsigned char v2;
unsigned char v3;
unsigned char color;

unsigned char tbuffer[7]; // Used for ASM PRINT

//###############################################################################################
//# # # SCREEN GAME MAP
//###############################################################################################

unsigned char scr_map[SCR_INDEX];
unsigned char scr_curr;

//#########################################################################################
//# # # CONTROL VARIABLES
//# #
//#
//###############################################################################################

uint16_t (*joyfunc1)(udk_t *); // pointer to joystick function Player 1
uint16_t (*joyfunc2)(udk_t *); // pointer to joystick function for game_control_mode
udk_t k1;
udk_t k2;

unsigned char dirs;
unsigned char dirs_last;

const unsigned int game_max_index = SCR_INDEX;

// SPRITES GAME ARRAYS
unsigned char class[GAME_MAX_SPRITES]; // CLASS OF SPRITE
 // SPRITE STATES SEE DEFINES UPPER BIT VALUES
unsigned char state[GAME_MAX_SPRITES];
unsigned char direction[GAME_MAX_SPRITES];
unsigned char last_col[GAME_MAX_SPRITES];
unsigned char last_lin[GAME_MAX_SPRITES];

 // MISC SPRITE VALUES  MIN COL/LIN
unsigned char value_a[GAME_MAX_SPRITES];
unsigned char value_b[GAME_MAX_SPRITES];
unsigned char value_c[GAME_MAX_SPRITES];
unsigned char value_d[GAME_MAX_SPRITES];
unsigned char value_e[GAME_MAX_SPRITES];

unsigned char spr_tile[GAME_MAX_SPRITES];

unsigned char spr_speed[GAME_MAX_SPRITES];

unsigned int last_time[GAME_MAX_SPRITES];   // LAST TIME OF MOVEMENT FOR ANIMATIONS / SPEED
unsigned int last_time_a[GAME_MAX_SPRITES]; // LAST TIME A
unsigned int last_time_b[GAME_MAX_SPRITES]; // LAST TIME B

unsigned char spr_frames[GAME_MAX_SPRITES];
unsigned char spr_lin_inc[GAME_MAX_SPRITES];
unsigned char spr_altset[GAME_MAX_SPRITES];
unsigned char spr_kind[GAME_MAX_SPRITES];

unsigned char tile[GAME_MAX_SPRITES]; // TILE
unsigned char lin[GAME_MAX_SPRITES];  // LINE

unsigned char col[GAME_MAX_SPRITES];      // COLUMN
unsigned char colint[GAME_MAX_SPRITES];   // INTERNAL COLUMN/TILE INCREMENT


unsigned char obj_lin[MAX_OBJECTS]; // object lin for HIGHLIGHT
unsigned char obj_col[MAX_OBJECTS]; // object col for HIGHLIGHT

unsigned char obj_count;

// PLAYER ONLY
uint64_t player_score; // SCORE
unsigned char player_lives;
unsigned char player_coins;
signed int player_vel_y;
signed int player_vel_y0;
signed char player_vel_inc;

unsigned char player_col_scr;
unsigned char player_lin_scr;

unsigned int player_kill_index;


signed int game_gravity;
unsigned char game_song_play;
//unsigned char game_song_play_start;
unsigned char game_conveyor_dir;
unsigned char game_conveyor_lin;
unsigned char game_conveyor_col0;
unsigned char game_conveyor_col1;
unsigned char game_exit_col;
unsigned char game_exit_lin;
unsigned char game_code;
unsigned char game_menu;
unsigned char game_effect;
unsigned char game_tune;
unsigned char game_beep;
unsigned char game_object_count;


unsigned char game_tileset;
unsigned char game_atrac;
unsigned char game_tunes;

unsigned char game_round_up;
unsigned char menu_curr_sel;

unsigned int fps;

unsigned char sprite;
unsigned char game_debug;

unsigned char s_lin0;
unsigned char s_lin1;
unsigned char s_col0;
unsigned char s_colint0;
unsigned char s_col1;
unsigned char s_row1;
unsigned char s_tile1;


unsigned int loop_count;

unsigned int index0;
unsigned int index1;

unsigned char zx_val_asm;
unsigned char attrib[4];
unsigned char attrib_1[4];

//unsigned char attrib_sol0[4];
//unsigned char attrib_sol1[4];
unsigned char g_ray1;



// TILE ATTRIB TODO REMOVE UNUSED
unsigned char key_attrib[4];


unsigned char *gbyte;



unsigned int curr_time;
unsigned int time_event;
unsigned int time_air;
unsigned int time_conv;
unsigned int time_key;

unsigned char key_last;
unsigned char key_ink;

unsigned char spr_count;


//###############################################################################################
//# # # GAME VARIABLES
//# #
//#
//###############################################################################################

unsigned char game_inmune;
unsigned char game_inf_lives;

unsigned char game_sound;
unsigned char game_tile_cnt;

unsigned char game_over;



uint64_t game_score_top;
uint64_t player_next_extra;
unsigned char game_start_scr;

// PHASE RELATED VARIABLES
unsigned char screen_paper;
unsigned char screen_ink;

unsigned char map_border;
unsigned char map_paper;
unsigned char map_clear;




//28*9
const unsigned char spr_init[GAME_ENEMY_MAX_CLASSES*GAME_ENEMY_CLASS_LEN];



unsigned char *attribs;
unsigned char *deltas;

const unsigned char tile_class[32];

//Hack for horizontal jump increment

unsigned char player_jump_count;
unsigned char player_jump_lin;
unsigned char player_jump_top;
unsigned char player_fall_start;
unsigned char player_eat_count;



//Jump Geometry HORIZONTAL extra increment to emulate Willy Jump

const unsigned char player_jump_hor[] = {
  0, //0
  0, //1
  0, //2
  0, //3
  1, //4
  0, //5
  1, //6
  1, //7
  0, //8
  1, //9
  0, //10
  0, //11
  1, //12
  0, //13
  0, //14
  1, //15
  0  //16
};

const unsigned char game_encode[10] = {
  'Z', //0
  'X', //1
  'S', //2
  'P', //3
  'E', //4
  'C', //5
  'T', //6
  'R', //7
  'U', //8
  'M'  //9
};





const unsigned char tile_class[] = {

    TILE_EMPTY,  //00
    TILE_EMPTY,  //01
    TILE_DOT,    //02
    TILE_EMPTY,  //03

    TILE_EMPTY,   //04
    TILE_EMPTY,   //05
    TILE_EMPTY,   //06
    TILE_EMPTY,   //07

    TILE_EMPTY,    //08
    TILE_WALL,   //09
    TILE_EMPTY,   //10
    TILE_WALL,    //11

    TILE_WALL,     //12
    TILE_WALL,     //13
    TILE_WALL,     //14
    TILE_WALL,     //15

    TILE_WALL,     //16
    TILE_WALL,     //17
    TILE_WALL,     //18
    TILE_WALL,     //19

    TILE_WALL,     //20
    TILE_WALL,     //21
    TILE_WALL,     //22
    TILE_WALL,     //23

    TILE_WALL,     //24
    TILE_WALL,     //25
    TILE_WALL,     //26
    TILE_WALL,     //27

    TILE_WALL,     //28
    TILE_WALL,    //29 Ghost Entry
    TILE_PILL,    //30
    TILE_EMPTY,    //31
};


const unsigned char spr_init[] = {
    64,  24, 4,   4, E_HORIZONTAL, 0xFF, 0xFF, 0xFF, 0xFF, // GHOST CYAN
    66,  32, 4,   4, E_HORIZONTAL, 0x05, 0x07, 0xFF, 0xFF, // GHOST2
    68,  40, 4,   4, E_HORIZONTAL, 0x03, 0x02, 0x04, 0xFF, // GHOST3
    70,  48, 4,   4, E_HORIZONTAL, 0x03, 0x05, 0xFF, 0xFF, // GHOST4
};
/*
Stage	Fruit	Fruit Points	Ghost Blue Time (seconds)	Flashes before Blue Time ends
1	Cherry	100	6	5
2	Strawberry	300	5	5
3	Orange	500	4	5
4	Orange	500	3	5
5	Apple	700	2	5
6	Apple	700	5	5
7	Melon	1000	2	5
8	Melon	1000	2	5
9	Galaxian	2000	1	3
10	Galaxian	2000	5	5
11	Bell	3000	2	5
12	Bell	3000	1	3
13	Key	5000	1	3
14	Key	5000	3	5
15	Key	5000	1	3
16	Key	5000	1	3
17	Key	5000	0	–
18	Key	5000	1	3
19 and beyond	Key	5000	0	–
*/

//Fruit Target Icon
const unsigned char scr_fruit[] = {
    96, // 0 Cherry
    97, // 1 Strawberry
    98, // 2 Orange
    98, // 3 Orange
    99, // 4 Apple
    99, // 5 Apple
   100, // 6 Melon
   100, // 7 Melon
   101, //8 Galaxian
   101, //9 Galaxian
   102, //10 Bell
   102, //11 Bell
   103, //12 Key
   103, //13 Key
   103, //14 Key
   103, //15 Key
   103, //16 Key
   103, //17 Key
   103  //18 Key
};

//Ghost Speed
const unsigned char scr_speed[] = {
    5, // 0 Cherry
    5, // 1 Strawberry
    4, // 2 Orange
    4, // 3 Orange
    3, // 4 Apple
    3, // 5 Apple
    2, // 6 Melon
    2, // 7 Melon
    1, //8 Galaxian
    1, //9 Galaxian
    0, //10 Bell
    0, //11 Bell
    0, //12 Key
    0, //13 Key
    0, //14 Key
    0, //15 Key
    0, //16 Key
    0, //17 Key
    0  //18 Key
};

//Fruit Score
const unsigned int scr_score[] = {
    10, // 0 Cherry
    30, // 1 Strawberry
    50, // 2 Orange
    50, // 3 Orange
    70, // 4 Apple
    70, // 5 Apple
   100, // 6 Melon
   100, // 7 Melon
   200, //8 Galaxian
   200, //9 Galaxian
   500, //10 Bell
   500, //11 Bell
   500, //12 Key
   500, //13 Key
   500, //14 Key
   500, //15 Key
   500, //16 Key
   500, //17 Key
   500  //18 Key
};

//Blue Time
unsigned int scr_blue_time;
const unsigned char scr_blue[] = {
   6, // 0 Cherry
   5, // 1 Strawberry
   4, // 2 Orange
   3, // 3 Orange
   2, // 4 Apple
   5, // 5 Apple
   2, // 6 Melon
   2, // 7 Melon
   1, //8 Galaxian
   5, //9 Galaxian
   2, //10 Bell
   1, //11 Bell
   1, //12 Key
   3, //13 Key
   1, //14 Key
   1, //15 Key
   0, //16 Key
   1, //17 Key
   0  //18 Key
};

//Blink Time
unsigned int scr_blink_time;
const unsigned char scr_blink[] = {
   5, // 0 Cherry
   5, // 1 Strawberry
   5, // 2 Orange
   5, // 3 Orange
   5, // 4 Apple
   5, // 5 Apple
   5, // 6 Melon
   5, // 7 Melon
   3, //8 Galaxian
   5, //9 Galaxian
   5, //10 Bell
   3, //11 Bell
   3, //12 Key
   5, //13 Key
   3, //14 Key
   3, //15 Key
   0, //16 Key
   3, //17 Key
   0  //18 Key
};

//SPR8 LOOKUP TABLES
unsigned char *spr8_lookup_s[32];
unsigned char *spr8_lookup_e[32];
