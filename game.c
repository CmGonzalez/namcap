/*
        This file is part of NamCap.

        NamCap is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        NamCap is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with NamCap.  If not, see <http://www.gnu.org/licenses/>.


        NamCap - Cristian Gonzalez - cmgonzalez@gmail.com
 */

#include "game.h"
#include "game_audio.h"
#include "game_banks.h"
#include "game_enemies.h"
#include "game_engine.h"
#include "game_menu.h"
#include "game_player.h"
#include "game_sprite.h"
#include "game_zx.h"
#include "macros.h"
#include <arch/zx.h>
#include <arch/zx/nirvana+.h>
#include <input.h>
#include <stdlib.h>

void main(void) {

  zx_border(INK_BLACK);
  zx_print_ink(INK_WHITE);
  // DEBUG
  game_debug = 1;

  game_song_play = 1;
  game_gravity = 100; // GAME_GRAVITY;
  game_inmune = 0;    // GAME_INMUNE;
  game_inf_lives = 0; // GAME_INF_LIVES;
  // Nirvana Attributes lookup tables
  attribs = (unsigned char *)0xFCC8;
  deltas = (unsigned char *)0xFF01;
  scr_curr = 0;
#ifdef DEBUG
  game_inmune = 0;    // GAME_INMUNE;
  game_inf_lives = 1; // GAME_INF_LIVES;
  game_menu = 0;
  menu_curr_sel = 2; // Sync Menu
  game_tune = 4;
#else
  z80_delay_ms(666); // SATANIC DELAY
  game_menu = 1;
  menu_curr_sel = 1; // Sync Menu
  game_effect = 1;
#endif

  // INTERRUPTS ARE DISABLED
  // RESET AY CHIP
  ay_reset();

  // Init Game
  game_start_timer();

  // ENABLE SOUND BASED ON DETECTED MODEL
  game_sound = spec128 ? (GAME_SOUND_AY_FX_ON | GAME_SOUND_AY_MUS_ON)
                       : (GAME_SOUND_48_FX_ON | GAME_SOUND_48_MUS_ON);

  // Keyboard Handling
  k1.fire = IN_KEY_SCANCODE_SPACE;//TODO DEBUG IN_KEY_SCANCODE_DISABLE;
  // TODO k1.fire1 = IN_KEY_SCANCODE_SPACE;
  k1.left = IN_KEY_SCANCODE_o;
  k1.right = IN_KEY_SCANCODE_p;
  // must be defined otherwise up is always true
  k1.up = IN_KEY_SCANCODE_q;
  k1.down = IN_KEY_SCANCODE_a;
#ifdef DEBUG
  joyfunc1 = (uint16_t(*)(udk_t *))(in_stick_keyboard);
#else
  joyfunc1 = (uint16_t(*)(udk_t *))(in_stick_sinclair1);
#endif

  k2.left = IN_KEY_SCANCODE_CAPS;
  k2.right = IN_KEY_SCANCODE_SYM;

  k2.up = IN_KEY_SCANCODE_DISABLE;
  k2.down = IN_KEY_SCANCODE_DISABLE;
  k2.fire = IN_KEY_SCANCODE_DISABLE;
  joyfunc2 = (uint16_t(*)(udk_t *))(in_stick_keyboard);

#ifndef DEBUG
  for (loop_count = 31416; !in_test_key(); loop_count += 10061)
    ;
  srand(loop_count);
#endif

  // Clear Screen and init Nirvana
  zx_paper_fill(INK_BLACK | PAPER_BLACK);
  NIRVANAP_tiles(_btiles);

spr_fill_lookup_table();

  NIRVANAP_start();



  game_attribs();
  game_over = 1;

  // Init Screen
  time_event = zx_clock();
  game_score_top = 10000;

  while (1) {

    game_cls();

    // MENU
    if (game_menu) {
      menu_main();
    }
    // GAME
    game_cls();
    player_next_extra = 10000;
    scr_curr = 0;
    game_loop();
    game_cls();
    game_paint_attrib(&attrib, 0, 31, (11 << 3) + 8);
    game_over = 0; // Hack game_colour_message to render background
    // GAME OVER


  }
}
