;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; IM2 INTERRUPT SERVICE ROUTINE ;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SECTION code_crt_common

PUBLIC _zx_isr, _BeepFX_play

EXTERN _spec128, _GLOBAL_ZX_PORT_7FFD

EXTERN _ay_song_bank, _ay_song_command, _ay_song_command_param
EXTERN vtii_setup, vtii_stop, vtii_init, vtii_play

EXTERN _ay_fx_bank, _ay_fx_command, _ay_fx_command_param
EXTERN mfx_mfxptr, mfx_add, mfx_playm

_zx_isr:

   ; check for 128k model

   ld a,(_spec128)
   or a
   ret z

   ; avoid contention problems

   ld a,0x80
   ld i,a

   ; process song command

song_command:

   ld a,(_ay_song_bank)
   or $10

   ld bc,$7ffd
   out (c),a                   ; page in song bank

   ld hl,vtii_setup
   set 1,(hl)                  ; enable stack output from ay song player

   ld a,(_ay_song_command)

   bit 1,a
   jr z, check_song_new

song_stop:

   call vtii_stop
   jr song_exit

check_song_new:

   bit 2,a
   jr z, song_play

song_new:

   and $01                     ; keep loop bit
   or $02                      ; enable stack output from ay song player
   ld (vtii_setup),a

   ld hl,(_ay_song_command_param)

   call vtii_init
   jr song_exit

song_play:

   call vtii_play

song_exit:

   xor a
   ld (_ay_song_command),a

   ; process fx command

fx_command:

   ld a,(_ay_fx_bank)
   or $10

   ld bc,$7ffd
   out (c),a                   ; page in fx bank

   ld a,(_ay_fx_command)

   bit 1,a
   jr z, check_fx_new

fx_stop:

   ld hl,0
   ld (mfx_mfxptr+1),hl

   jr fx_play

check_fx_new:

   bit 2,a
   jr z, fx_play

fx_new:

   ld hl,(_ay_fx_command_param)
   call mfx_add

fx_play:

   call mfx_playm

fx_exit:

   xor a
   ld (_ay_fx_command),a

   ; restore original bank

   ld a,(_GLOBAL_ZX_PORT_7FFD)

   ld bc,$7ffd
   out (c),a

   ; restore I

   ld a,0xfe
   ld i,a

   ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PRINTING UTILITIES ;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SECTION code_user

; void zx_print_str(unsigned char ui_row, unsigned char ui_col, unsigned char *s)
; callee linkage

PUBLIC _zx_print_str

EXTERN asm_zx_cxy2saddr, asm_zx_saddr2aaddr
EXTERN _game_font, _screen_ink, _screen_paper

_zx_print_str:

IF __SDCC

   pop af
   pop hl                      ; l = row, h = col
   pop de                      ; de = s
   push af

   ld a,l
   ld l,h
   ld h,a

ENDIF

IF __SCCZ80

   pop hl
   pop de                      ; de = s
   pop bc                      ; c = col
   ex (sp),hl                  ; l = row

   ld h,l
   ld l,c

ENDIF

zx_print_str:

   ;  h = y coord
   ;  l = x coord
   ; de = char *s

   call asm_zx_cxy2saddr       ; z88dk function: character y,x to screen address
   ex de,hl

zps_sloop:

   ; de = char *screen
   ; hl = char *s

   ld a,(hl)

   or a
   ret z

   push de
   push hl

   ld l,a
   ld h,0

   ; hl = char c
   ; de = char *screen
   ; stack = char *screen, char *s

   add hl,hl
   add hl,hl
   add hl,hl
   ld bc, _game_font - 256
   add hl,bc

   ld b,8

zps_cloop:

   ld a,(hl)
   ld (de),a
   inc hl
   inc d

   djnz zps_cloop

   dec d
   ex de,hl

   call asm_zx_saddr2aaddr     ; z88dk function: screen address to attribute address

   ld a,(_screen_ink)
   ld b,a
   ld a,(_screen_paper)
   or b

   ld (hl),a

   pop hl
   pop de

   inc e
   inc hl

   jr zps_sloop


; void zx_print_int(unsigned char ui_row, unsigned char ui_col, unsigned int val)
; callee linkage

PUBLIC _zx_print_int

EXTERN l_small_utoa, _tbuffer

_zx_print_int:

IF __SDCC

   pop af
   pop de                      ; e = row, d = col
   pop hl                      ; hl = val
   push af

   ld a,e
   ld e,d
   ld d,a

ENDIF

IF __SCCZ80

   pop af
   pop hl                      ; hl = val
   pop de                      ; e = col
   pop bc                      ; c = row
   push af

   ld d,c

ENDIF

zx_print_int:

   ;  e = col
   ;  d = row
   ; hl = val

   push de
   ld de,_tbuffer

   ; hl = unsigned int val
   ; de = char *buffer
   ; stack = row/col

   scf                         ; request leading zeroes
   call l_small_utoa           ; z88dk function: unsigned int to ascii buffer

   ; ld a,'0'

   ; ld (de),a                   ; add trailing zero to multiply scores by 10
   ; inc de

   xor a
   ld (de),a                   ; zero terminate

   pop hl
   ld de,_tbuffer

   ; de = char *buffer
   ;  h = row
   ;  l = col

   jp zx_print_str


; void zx_print_chr(unsigned char ui_row, unsigned char ui_col, unsigned char val)
; callee linkage

PUBLIC _zx_print_chr

EXTERN l_small_utoa, _tbuffer

_zx_print_chr:

IF __SDCC

   pop hl
   dec sp
   pop de                      ; d = row
   ex (sp),hl                  ; l = col, h = val

   ld e,l
   ld l,h
   ld h,0

ENDIF

IF __SCCZ80

   pop af
   pop hl                      ; hl = val
   pop de                      ; e = col
   pop bc                      ; c = row
   push af

   ld d,c

ENDIF

zx_print_chr:

   ;  e = col
   ;  d = row
   ; hl = val

   push de
   ld de,_tbuffer

   ; hl = unsigned int val
   ; de = char *buffer
   ; stack = row/col

   scf                         ; request leading zeroes
   call l_small_utoa           ; z88dk function: unsigned int to ascii buffer

   xor a
   ld (de),a                   ; zero terminate

   pop hl
   ld de,_tbuffer+2            ; print last three digits only

   ; de = char *buffer
   ;  h = row
   ;  l = col

   jp zx_print_str



   ;BeepFX player by Shiru
   ;You are free to do whatever you want with this code



   playBasic:
   	ld a,0
 _BeepFX_play:
    play:
   	ld hl,sfxData	;address of sound effects data

   	;di NOENTIENDO
   	push ix
   	push iy

   	ld b,0
   	ld c,a
   	add hl,bc
   	add hl,bc
   	ld e,(hl)
   	inc hl
   	ld d,(hl)
   	push de
   	pop ix			;put it into ix

    ;NOENTIENDO comentado
   	;ld a,(23624)	;get border color from BASIC vars to keep it unchanged
   	;rra
   	;rra
   	;rra
   	;and 7
    ;NOENTIENDO fin comentado

   	ld (sfxRoutineToneBorder  +1),a
   	ld (sfxRoutineNoiseBorder +1),a
   	ld (sfxRoutineSampleBorder+1),a


   readData:


   	ld a,(ix+0)		;read block type
   	ld c,(ix+1)		;read duration 1
   	ld b,(ix+2)
   	ld e,(ix+3)		;read duration 2
   	ld d,(ix+4)

   	push de
   	pop iy

   	dec a
   	jr z,sfxRoutineTone
   	dec a
   	jr z,sfxRoutineNoise
   	dec a
   	jr z,sfxRoutineSample
    ei
   	pop iy
   	pop ix
   	;ei NOENTIENDO
   	ret



   ;play sample

sfxRoutineSample:

   	ex de,hl
sfxRS0:
ei
   	ld e,8 ;NOENTIENDO 8
   	ld d,(hl)
   	inc hl
sfxRS1:
ei
   	ld a,(ix+25)
    ld a,0x2

sfxRS2:


   	dec a

   	jr nz,sfxRS2

   	rl d
   	sbc a,a
   	and 16
sfxRoutineSampleBorder:
   	or 0
    and 248;NOENTIENDO border0

   	out (254),a

    dec e

   	jr nz, sfxRS1

   	dec bc
   	ld a,b
   	or c
   	jr nz,sfxRS0

   	ld c,6


   nextData:

    add ix,bc		;skip to the next block

   	jr readData



   ;generate tone with many parameters

   sfxRoutineTone:
   	ld e,(ix+5)		;freq
   	ld d,(ix+6)
   	ld a,(ix+9)		;duty
   	ld (sfxRoutineToneDuty+1),a
   	ld hl,0

   sfxRT0:
   	push bc
   	push iy
   	pop bc
   sfxRT1:
   	add hl,de
   	ld a,h
   sfxRoutineToneDuty:
   	cp 0
   	sbc a,a
   	and 16
   sfxRoutineToneBorder:
   	or 0
   	out (254),a

   	dec bc
   	ld a,b
   	or c
   	jr nz,sfxRT1

   	ld a,(sfxRoutineToneDuty+1)	 ;duty change
   	add a,(ix+10)
   	ld (sfxRoutineToneDuty+1),a

   	ld c,(ix+7)		;slide
   	ld b,(ix+8)
   	ex de,hl
   	add hl,bc
   	ex de,hl

   	pop bc
   	dec bc
   	ld a,b
   	or c
   	jr nz,sfxRT0

   	ld c,11
   	jr nextData



   ;generate noise with two parameters

sfxRoutineNoise:
   	ld e,(ix+5)		;pitch

   	ld d,1
   	ld h,d
   	ld l,d
sfxRN0:
   	push bc
   	push iy
   	pop bc
sfxRN1:
   	ld a,(hl)
   	and 16
sfxRoutineNoiseBorder:
   	or 0
   	out (254),a
   	dec d
   	jr nz,sfxRN2
   	ld d,e
   	inc hl
   	ld a,h
   	and 31
   	ld h,a

   sfxRN2:
   	dec bc
   	ld a,b
   	or c
   	jr nz,sfxRN1

   	ld a,e
   	add a,(ix+6)	;slide
   	ld e,a

   	pop bc
   	dec bc
   	ld a,b
   	or c
   	jr nz,sfxRN0

   	ld c,7
   	jr nextData



    sfxData:
include "sfx/NAMPAC.asm"
