/*
   This file is part of Pietro Engine v3.0.

   Pietro Engine is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Pietro Engine is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with NamCap.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLOBALS_H
#define GLOBALS_H
#include <input.h>

/* ENGINE DEFINES - Enable/Disable Engine Extra */
//#define DEBUG //Enable to enter debug mode


/* Screen Dimension Variables */
#define SCR_COLS                      32   // COLUMNS
#define SCR_ROWS                      26   // ROWS (+2 FAKE ROWS AT TOP, NIRVANA LIN 0 IS -8 PX)
#define SCR_INDEX                     832  // SCR_COLS * SCR_ROWS
#define LIN_FLOOR                     192  // ROWS*8



#define MAX_OBJECTS                  1 //OBJECTS OR COINS
#define GAME_MAX_SPRITES             6 //MAX OF OBJECTS (ENEMIES + PLAYERS)
#define GAME_MAX_ENEMIES             5 //GAME_MAX_SPRITES - PLAYERS
/* Player indexes */
#define INDEX_P1                     5 //Index of P1 == GAME_MAX_PRITES - 1
#define INDEX_FRUIT                  4 //Index of P1 == GAME_MAX_PRITES - 1

//Nirvana Related
#define NIRV_SPRITE_FRUIT            4 //Sprite to be used by P1 (Have transparency)
#define NIRV_SPRITE_P1               5 //Sprite to be used by P1 (Have transparency)

#define NIRV_TOTAL_SPRITES           7 //Maximun of sprites to handle (Nirvana == 8) (CUSTOM Nirvana == 7)

#define GAME_GRAVITY                 1
#define GAME_VELOCITY               -6
#define GAME_START_LIVES             3
#define GAME_COLLISION_TIME         30 //TIME BTWN COLISION CHECKS
#define CODELEN                      8 //LEN OF START CODES

#define PLAYER_SPEED                 2
#define PLAYER_SPEED_VJUMP           10
#define PLAYER_FALL_DEADLY           40
#define ENEMY_START_INDEX            64 //INDEX OF START OF ENEMY ON MAP ARRAY
#define ENEMY_NORMAL_SPEED           2
#define ENEMY_FAST_SPEED             0



//TILES CLASSES
#define TILE_EMPTY                   0  //BACKGROUND
#define TILE_FLOOR                   1  //PLATFORMS
#define TILE_CRUMB                   2  //PLATFORMS CRUMBLING
#define TILE_WALL                    3  //SOLID WALL
#define TILE_CONVEYOR                4  //CONVEYORS
#define TILE_DEADLY                  5  //DEADLY
#define TILE_DOT                     6  //EXTRA SWITCH
#define TILE_OBJECT                  8  //KEY
#define TILE_PILL                    9  //PILL
#define TILE_CRUMB_INIT              2 //TODO MOVE ON BTILE AND PLACE ADJACENT TO CRUMBLING TILES

#define GAME_TOTAL_CLASSES            6 //Total class of enemies
#define GAME_TOTAL_INDEX_CLASSES      30 //Total class of enemies variations, the same enemy can be left/right etc...
#define GAME_SPR_INIT_SIZE            9
#define GAME_MAX_VALUES               18 //Max Values for stage Data

#define E_NONE                        0
#define E_HORIZONTAL                  1
#define E_VERTICAL                    2
#define E_GHOST                       3
#define E_EYES                        4
#define E_WIMP                        5
#define E_WIMP_ALT                    6
#define E_FRUIT                       7
#define E_PLAYER                      255

#define TILE_TITLE                    204
#define TILE_ANIM                     128

#define GAME_ENEMY_MAX_CLASSES        30
#define GAME_ENEMY_CLASS_LEN          9




//TILE MAP'S
//  0..23   BACKGROUND
// 24..31   OBJECTS (CAN BE PICKED)
// 32..39   BACKGROUND DEADLY
// 40..47   PLATFORMS (FLOOR ONLY)
// 48..51   SPECIAL PLATFORMS (STAIRS)
// 52..55   BRICKS (FLOOR AND CEIL)
// 56..63   SPECIAL BRICKS (FLORR AND CEIL CAN BE HITTED)

/* Player 1 tiles */
#define TILE_P1_RIGHT                 8
#define TILE_P1_LEN                   4 //LEN OF PLAYER SPRITES FOR EACH DIRECTION
#define FRAMES_PLAYER                 4


#define ST_STAND_R                    0
#define ST_STAND_L                    1
#define ST_TURN_R                     10
#define ST_TURN_L                     11

//Starting Directions
#define DIR_NONE                      0
#define DIR_LEFT                      1
#define DIR_RIGHT                     2
#define DIR_UP                        3
#define DIR_DOWN                      4


/*Stats (bit position number)*/
#define STAT_DIRU                     0
#define STAT_DIRD                     1
#define STAT_DIRR                     2
#define STAT_DIRL                     3
#define STAT_WIMP                     4
#define STAT_WIMP_END                 5
#define STAT_KILLED                   6
#define STAT_LOCK                     7

/*Stats alt (bit position number)*/
#define STAT_A0                       0
#define STAT_A1                       1
#define STAT_A2                       2
#define STAT_A3                       3
#define STAT_A4                       4
#define STAT_A5                       5
#define STAT_A6                       6
#define STAT_A7                       7
/* General */
#define SPRITE_LIN_INC                2 //USED ONLY ONCE CHECK

/* Player */


/* Enemies */
#define ENEMIES_MAX                   5   //MAX QUANTITY OF ENEMIES ON SCREEN (0->6)
#define ENEMIES_MAXJUMP               12 //MAX JUMP WHEN HIT
#define ENEMY_JUMP_SPEED              1
#define ENEMY_FALL_SPEED              1
#define ENEMY_KILLED_SPEED            8


// Game timers
#define TIME_EVENT                    100
#define TIME_ANIM                     8
#define TIME_FRUIT                    500


// Sound Mode Variables
#define GAME_SOUND_48_FX_ON          0x01
#define GAME_SOUND_48_FX_OFF         0xfe
#define GAME_SOUND_48_MUS_ON         0x02
#define GAME_SOUND_48_MUS_OFF        0xfd
#define GAME_SOUND_AY_FX_ON          0x04
#define GAME_SOUND_AY_FX_OFF         0xfb
#define GAME_SOUND_AY_MUS_ON         0x08
#define GAME_SOUND_AY_MUS_OFF        0xf7


//typedef uint16_t (*JOYFUNC)(udk_t *);
#endif


extern unsigned char spec128;
//General Use VARIABLES
extern unsigned char i;
extern unsigned char v0;
extern unsigned char v1;
extern unsigned char v2;
extern unsigned char v3;

extern unsigned char btiles[];
extern uint16_t (*joyfunc1)(udk_t *); //TODO REMOVE THIS AS IS PART NOW OF input.h
extern uint16_t (*joyfunc2)(udk_t *); //TODO REMOVE THIS AS IS PART NOW OF input.h
extern udk_t k1;
extern udk_t k2;


extern unsigned char dirs;
extern unsigned char dirs_last;

extern unsigned char class[];
extern unsigned char state[];
extern unsigned char direction[];
extern unsigned char last_col[];
extern unsigned char last_lin[];
extern unsigned char value_a[];
extern unsigned char value_b[];
extern unsigned char value_c[];
extern unsigned char value_d[];
extern unsigned char value_e[];

extern unsigned char tile[GAME_MAX_SPRITES];
extern unsigned char lin[GAME_MAX_SPRITES];
extern unsigned char col[GAME_MAX_SPRITES];
extern unsigned char colint[GAME_MAX_SPRITES];

extern unsigned int last_time[GAME_MAX_SPRITES];
extern unsigned int last_time_a[GAME_MAX_SPRITES];
extern unsigned int last_time_b[GAME_MAX_SPRITES];

extern unsigned char obj_lin[MAX_OBJECTS]; // object lin for HIGHLIGHT
extern unsigned char obj_col[MAX_OBJECTS]; // object col for HIGHLIGHT
extern unsigned char obj_count;

extern unsigned char tbuffer[7];

extern uint64_t player_score;
extern signed int  player_vel_y;
extern signed int  player_vel_y0;
extern signed char  player_vel_inc;
extern unsigned char player_jump_hack;
extern unsigned char player_jump_count;
extern unsigned char player_jump_lin;
extern const unsigned char player_jump_hor[];
extern const unsigned char game_encode[];
extern unsigned char player_eat_count;


extern unsigned char player_fall_start;

//PLAYER ATTRIBUTES




extern unsigned char player_col_scr;
extern unsigned char player_lin_scr;



extern signed   int  game_gravity;
extern unsigned char game_song_play;
//extern unsigned char game_song_play_start;
extern unsigned char game_conveyor_dir;
extern unsigned char game_conveyor_lin;
extern unsigned char game_conveyor_col0;
extern unsigned char game_conveyor_col1;
extern unsigned char game_exit_col;
extern unsigned char game_exit_lin;
extern unsigned char game_code;

extern unsigned char game_round_up;
extern unsigned char menu_curr_sel;
extern unsigned char game_tileset;
extern unsigned char game_atrac;
extern unsigned char game_menu;
extern unsigned char game_effect;
extern unsigned char game_object_count;



extern unsigned int fps;



extern unsigned char sprite;
extern unsigned char game_debug;

extern unsigned char player_jump_top;

extern unsigned int  player_kill_index;


extern unsigned char s_lin0;
extern unsigned char s_col0;
extern unsigned char s_colint0;

extern unsigned char s_col1;
extern unsigned char s_lin1;
extern unsigned char s_row1;
extern unsigned char s_tile1;


extern unsigned int loop_count;
extern unsigned int index0;
extern unsigned int index1;

extern unsigned char zx_val_asm;

//Attrib Arrays
extern unsigned char attrib[4];
extern unsigned char attrib_1[4];




//TILE ATTRIB
extern unsigned char key_attrib[4];


//TODO REVIEW 128 bytes! needs loop on final target without udg's

extern unsigned int curr_time;
extern unsigned int time_event;

extern unsigned int time_air;
extern unsigned int time_conv;
extern unsigned int time_key;

extern unsigned char key_last;
extern unsigned char key_ink;

extern unsigned char spr_count;


extern unsigned char game_inmune;
extern unsigned char game_inf_lives;
extern unsigned char game_sound;
extern unsigned char game_tile_cnt;

extern unsigned char game_over;

extern unsigned char player_lives;
extern unsigned char player_coins;
extern uint64_t game_score_top;
extern uint64_t player_next_extra;
extern unsigned char game_start_scr;


extern unsigned char screen_paper, screen_ink;
extern const unsigned char spr_init[];


extern unsigned char spr_tile[];
extern unsigned char spr_speed[];

extern unsigned char spr_frames[];
extern unsigned char spr_lin_inc[];
extern unsigned char spr_altset[];
extern unsigned char spr_kind[];
extern unsigned char scr_map[];


//extern unsigned char scr_obj0[];
//extern unsigned char scr_obj1[];



//Bank 6 Level Data
extern const unsigned char world0[];
extern const unsigned int lenght0[];

extern unsigned char color;
extern unsigned char scr_curr;
extern unsigned char *gbyte;



extern unsigned char map_border;
extern unsigned char map_paper;
extern unsigned char map_clear;


extern const unsigned char tile_class[];


extern unsigned char *attribs;
extern unsigned char *deltas;


extern unsigned char game_tune;
extern unsigned char game_beep;
extern const unsigned int game_max_index;

extern unsigned char *spr8_lookup_s[];
extern unsigned char *spr8_lookup_e[];

extern const unsigned char scr_fruit[];
extern const unsigned char scr_speed[];
extern const unsigned int scr_score[];
extern unsigned int scr_blue_time;
extern const unsigned char scr_blue[];
extern unsigned int scr_blink_time;
extern const unsigned char scr_blink[];
