/*
	This file is part of NamCap.

	NamCap is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	NamCap is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with NamCap.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef GAME_GAME_H
#define GAME_GAME_H

#include "globals.h"
extern void			     game_start_timer(void);
extern void          game_tick(void);


extern void          game_draw_map(void);
extern void          game_fps(void);
extern void          game_cls(void);
extern void          game_fill_row(unsigned char, unsigned char) __z88dk_callee;
extern void			     game_update_stats(void);
extern void			     game_round_init(void);
extern void			     game_loop(void);
extern void          game_cell_paint();
extern void          game_cell_paint_index();
extern void			     game_print_score(void);
extern void          game_paint_attrib( unsigned char *f_attrib[], char f_start, unsigned char f_end, unsigned char f_lin);
extern void			     game_colour_message( unsigned char f_row, unsigned char f_col, unsigned char f_col2, unsigned int f_miliseconds, unsigned char skip);
extern void			     game_key_paint(void);
extern unsigned char game_check_time(unsigned int *, unsigned int);
extern void          game_attrib(unsigned char);
extern void          game_attribs(void);
extern void          game_page_map(void);
//TODO MOVE TO SPRITES
extern unsigned char game_copy_sprite_std(unsigned char f_hi_sprite, unsigned char f_low_sprite);
void                 game_copy_sprite_color_reset(void);
void                 game_copy_sprite(unsigned char f_hi_sprite, unsigned char f_low_sprite, unsigned char f_flip);
extern void          game_intro( );
extern void          page( unsigned char bank );
extern void          game_pause0();
extern void          game_beeper();
extern void          game_print_lives();
extern void          game_print_header();
extern void          game_print_footer();

extern unsigned char reverse(unsigned char b);
extern void          zx_print_char(unsigned char ui_row, unsigned char ui_col, unsigned char c);
extern void          game_sprite_restart(unsigned char);
extern void          game_sprite_reset();
extern void          game_scr_val();


#endif
