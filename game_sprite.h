/*
	This file is part of NamCap.

	NamCap is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	NamCap is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with NamCap.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef GAME_SPRITE_H
#define GAME_SPRITE_H
extern unsigned char    spr_chktime(void);
extern unsigned int	    spr_calc_index(unsigned char l, unsigned char c);
extern void             spr_set_up();
extern void             spr_set_down();
extern void             spr_set_left();
extern void             spr_set_right();

extern unsigned char    spr_move_up(void);
extern unsigned char    spr_move_down(void);

extern unsigned char    spr_move_right(void);
extern unsigned char    spr_move_left(void);

extern void             spr_paint(void);
extern void             spr_clear_fast(void);
extern void             spr_clear_fast_vert(void);
extern void             spr_clear_fast_hor(void);

extern unsigned char    spr_get_tile(unsigned char *) __z88dk_fastcall;
extern unsigned char    spr_get_tile_dir(unsigned char *, unsigned char *);
extern void             spr_back_repaint(void);
extern void             spr_back_repaint_dir(void);

extern void             spr_clear_scr(void);
extern void             spr_draw8(unsigned char, unsigned char, unsigned char);
extern void             spr_fill_lookup_table();

#endif
