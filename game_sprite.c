/*
        This file is part of NamCap.

        NamCap is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        NamCap is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with NamCap.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "game.h"
#include "game_audio.h"
#include "game_banks.h"
#include "game_enemies.h"
#include "game_engine.h"
#include "game_player.h"
#include "game_sprite.h"
#include "game_zx.h"
#include "macros.h"
#include <arch/zx.h>
#include <arch/zx/nirvana+.h>
#include <input.h>
#include <stdlib.h>

unsigned int spr_calc_index(unsigned char f_lin, unsigned char f_col) {
  // Mapeo a indice en scr_map 32x16 mapa
  // column + (line / 8) * 32
  return f_col + ((f_lin >> 3) << 5);
}

unsigned char spr_chktime(void) {
  if (zx_clock() - last_time[sprite] >= spr_speed[sprite]) {
    last_time[sprite] = zx_clock();
    return 1;
  }
  return 0;
}

void spr_set_up() {
  if (direction[sprite] == DIR_LEFT || direction[sprite] == DIR_RIGHT) {
    colint[sprite] = 0;
  }
  direction[sprite] = DIR_UP;
}
void spr_set_down() {
  if (direction[sprite] == DIR_LEFT || direction[sprite] == DIR_RIGHT) {
    colint[sprite] = 3;
  }
  direction[sprite] = DIR_DOWN;
}

void spr_set_left() {
  if (direction[sprite] == DIR_UP || direction[sprite] == DIR_DOWN) {
    colint[sprite] = 3;
  }
  direction[sprite] = DIR_LEFT;
}

void spr_set_right() {
  if (direction[sprite] == DIR_UP || direction[sprite] == DIR_DOWN) {
    colint[sprite] = 0;
  }
  direction[sprite] = DIR_RIGHT;
}

unsigned char spr_move_up(void) {

  ++colint[sprite];
  if (colint[sprite] >= spr_frames[sprite]) {
    colint[sprite] = 0;
  };
  if (lin[sprite] <= (LIN_FLOOR + spr_lin_inc[sprite])) { // Overflow
    lin[sprite] = lin[sprite] - spr_lin_inc[sprite];
  }

  return 0;
}

unsigned char spr_move_down(void) {

  ++colint[sprite];
  if (colint[sprite] >= spr_frames[sprite]) {
    colint[sprite] = 0;
  };
  if (lin[sprite] <= (LIN_FLOOR - spr_lin_inc[sprite])) {
    lin[sprite] = lin[sprite] + spr_lin_inc[sprite];
  }

  return 0;
}

unsigned char spr_move_left(void) {
  --colint[sprite];
  if (colint[sprite] >= spr_frames[sprite]) {

    --col[sprite];
    if (col[sprite] < 1) {
      NIRVANAP_spriteT(sprite, 0, 0, 0);
      NIRVANAP_halt();
      NIRVANAP_fillT(PAPER_BLACK | INK_BLACK, s_lin0, s_col0);

      col[sprite] = 30;
      s_col0 = 30;
    }
    colint[sprite] = spr_frames[sprite] - 1;

    return 1;
  }
  return 0;
}

unsigned char spr_move_right(void) {

  ++colint[sprite];
  if (colint[sprite] >= spr_frames[sprite]) {
    ++col[sprite];
    if (col[sprite] > 30) {
      NIRVANAP_spriteT(sprite, 0, 0, 0);
      NIRVANAP_halt();
      NIRVANAP_fillT(PAPER_BLACK | INK_BLACK, s_lin0, s_col0);

      col[sprite] = 0;
      s_col0 = 0;
    }
    colint[sprite] = 0;
    return 1;
  }
  return 0;
}

void spr_draw8(unsigned char f_spr8, unsigned char f_lin, unsigned char f_col) {
  unsigned char *f_byte_src0;

  f_byte_src0 = &btiles[0] + (48 * (f_spr8 >> 2));
  switch (f_spr8 & 3) {
  case 0:
    NIRVANAP_printQ(f_byte_src0, f_byte_src0 + 32, f_lin, f_col);
    break;
  case 1:
    NIRVANAP_printQ(f_byte_src0 + 1, f_byte_src0 + 40, f_lin, f_col);
    break;
  case 2:
    NIRVANAP_printQ(f_byte_src0 + 16, f_byte_src0 + 36, f_lin, f_col);
    break;
  case 3:
    NIRVANAP_printQ(f_byte_src0 + 17, f_byte_src0 + 44, f_lin, f_col);
    break;
  }
}

void spr_clear_scr() {
  unsigned char i;
  unsigned char j;
  zx_print_ink(PAPER_BLACK | INK_BLACK);
  //game_fill_row(0, 22);
  // Clear Sprites
  for (i = 0; i < NIRV_TOTAL_SPRITES; ++i) {
    NIRVANAP_spriteT(i, TILE_EMPTY, 0, 0);
  }

  for (i = 0; i < 8; ++i) {

    j = 0;
    v0 = i << 1;
    zx_print_str(0,v0,"  ");
    while (j <= LIN_FLOOR) {
      NIRVANAP_fillT(INK_BLACK || PAPER_BLACK, j, v0);
      j = j + 16;
    }
    zx_print_str(23,v0,"  ");
    j = 0;
    v0 = 30 - (i << 1);
    zx_print_str(0,v0,"  ");
    while (j <= LIN_FLOOR) {
      NIRVANAP_fillT(INK_BLACK || PAPER_BLACK, j, v0);
      j = j + 16;
    }
    zx_print_str(23,v0,"  ");
    NIRVANAP_halt();

  }

  //game_fill_row(23, 22);
}

void spr_paint(void) {
  NIRVANAP_spriteT(sprite, tile[sprite] + colint[sprite], lin[sprite],
                   col[sprite]);
}

void spr_clear_fast() {
  // Method0 Using spr_draw8
  v2 = s_lin0 + 8;
  v3 = s_col0 + 1;
  spr_draw8(0, v2, s_col0);
  spr_draw8(0, v2, v3);
  v2 = v2 + 8;
  spr_draw8(0, v2, s_col0);
  spr_draw8(0, v2, v3);
}

void spr_clear_fast_hor(void) {
  // if (BIT_CHK(state[sprite], STAT_DIRR)) {
  if (direction[sprite] == DIR_RIGHT) {
    v1 = s_col0;
  } else {
    v1 = s_col0 + 1;
  }
  v0 = s_lin0;
  NIRVANAP_paintC(attrib_1, v0, v1);
  v0 = v0 + 8;
  NIRVANAP_paintC(attrib_1, v0, v1);
}

void spr_clear_fast_vert(void) {

  unsigned char *f_byte;
  if (direction[sprite] == DIR_DOWN) {
    f_byte = zx_py2saddr(s_lin0 + 8) + s_col0;
    *f_byte = 0;
    ++f_byte;
    *f_byte = 0;
    f_byte += 255;
    *f_byte = 0;
    ++f_byte;
    *f_byte = 0;
    //*(f_byte + 1) = 0;
    //*(f_byte + 256) = 0;
    //*(f_byte + 257) = 0;
  } else {
    f_byte = zx_py2saddr(s_lin0 + 8 + 14) + s_col0; // TODO WHY 14???
    *f_byte = 0;
    ++f_byte;
    *f_byte = 0;
    f_byte += 255;
    *f_byte = 0;
    ++f_byte;
    *f_byte = 0;
  }
}

unsigned char spr_get_tile(unsigned char *f_sprite) __z88dk_fastcall {
  // Search enemy class associated Values
  return spr_get_tile_dir(&spr_tile[*f_sprite], &spr_altset[*f_sprite]);
}

unsigned char spr_get_tile_dir(unsigned char *f_tile, unsigned char *f_inc) {

  if (direction[sprite] == DIR_RIGHT) {
    return *f_tile;
  }

  if (direction[sprite] == DIR_LEFT) {
    return *f_tile + *f_inc;
  }

  if (direction[sprite] == DIR_UP) {
    return *(f_tile) + (*f_inc) * 2;
  }
  if (direction[sprite] == DIR_DOWN) {
    return *(f_tile) + (*f_inc) * 3;
  }

  return *f_tile;
}

void spr_back_repaint_dir(void) {
  if (col[sprite] != s_col0 || lin[sprite] != s_lin0) {
    // spr_back_repaint();
    switch (direction[sprite]) {
    case DIR_UP:
      // TODO A little 2p trail is seen
      // v0 = ((s_lin0+16) >> 3) << 3;
      // spr_draw8(scr_map[index0 + 64], v0, s_col0);
      // spr_draw8(scr_map[index0 + 65], v0, s_col0 + 1);

      if ((s_lin0 & 7) == 0) {
        s_lin1 = ((s_lin0 + 8) >> 3) << 3;
        s_tile1 = scr_map[index0 + 32];
        NIRVANAP_printQ(spr8_lookup_s[s_tile1], spr8_lookup_e[s_tile1], s_lin1,
                        s_col0);
        s_tile1 = scr_map[index0 + 33];
        NIRVANAP_printQ(spr8_lookup_s[s_tile1], spr8_lookup_e[s_tile1], s_lin1,
                        s_col0 + 1);
      } else {
        s_lin1 = ((s_lin0 + 16) >> 3) << 3;
        s_tile1 = scr_map[index0 + 64];
        NIRVANAP_printQ(spr8_lookup_s[s_tile1], spr8_lookup_e[s_tile1], s_lin1,
                        s_col0);
        s_tile1 = scr_map[index0 + 65];
        NIRVANAP_printQ(spr8_lookup_s[s_tile1], spr8_lookup_e[s_tile1], s_lin1,
                        s_col0 + 1);
      }

      break;
    case DIR_DOWN:
      // v0 = ((s_lin0 ) >> 3) << 3;
      // spr_draw8(scr_map[index0], v0, s_col0);
      // spr_draw8(scr_map[index0 + 1], v0, s_col0 + 1);
      s_lin1 = ((s_lin0) >> 3) << 3;
      s_tile1 = scr_map[index0];
      NIRVANAP_printQ(spr8_lookup_s[s_tile1], spr8_lookup_e[s_tile1], s_lin1,
                      s_col0);
      s_tile1 = scr_map[index0 + 1];
      NIRVANAP_printQ(spr8_lookup_s[s_tile1], spr8_lookup_e[s_tile1], s_lin1,
                      s_col0 + 1);
      break;
    case DIR_LEFT:
      // spr_draw8(scr_map[index0 + 1], s_lin0, s_col0 + 1);
      // spr_draw8(scr_map[index0 + 33], s_lin0 + 8, s_col0 + 1);
      s_col1 = s_col0 + 1;
      s_tile1 = scr_map[index0 + 1];
      NIRVANAP_printQ(spr8_lookup_s[s_tile1], spr8_lookup_e[s_tile1], s_lin0,
                      s_col1);
      s_tile1 = scr_map[index0 + 33];
      NIRVANAP_printQ(spr8_lookup_s[s_tile1], spr8_lookup_e[s_tile1],
                      s_lin0 + 8, s_col1);
      break;
    case DIR_RIGHT:
      // spr_draw8(scr_map[index0], s_lin0, s_col0);
      // spr_draw8(scr_map[index0 + 32], s_lin0 + 8, s_col0);
      s_tile1 = scr_map[index0];
      NIRVANAP_printQ(spr8_lookup_s[s_tile1], spr8_lookup_e[s_tile1], s_lin0,
                      s_col0);
      s_tile1 = scr_map[index0 + 32];
      NIRVANAP_printQ(spr8_lookup_s[s_tile1], spr8_lookup_e[s_tile1],
                      s_lin0 + 8, s_col0);
      break;
    }
  }
}

void spr_back_repaint(void) {
  unsigned int f_index;
  unsigned char f_lin;
  unsigned char f_tile;
  // Repaints the Backgound by reading back from scr_map the needed tile_class
  // We paint 4 tiles if the sprite is on 8 multiples coordinates
  // if the sprite is not on 8 multiple line, We paint 2 extra tiles to avoid
  // corruption

  f_index = index0;
  f_lin = (s_lin0 >> 3) << 3;
  s_col1 = s_col0 + 1;
  // UP RIGHT
  // spr_draw8(scr_map[f_index], f_lin, s_col0);
  f_tile = scr_map[f_index];
  NIRVANAP_printQ(spr8_lookup_s[f_tile], spr8_lookup_e[f_tile], f_lin, s_col0);

  // UP Left
  // spr_draw8(scr_map[f_index++], f_lin, s_col1);
  f_index++;
  f_tile = scr_map[f_index];
  NIRVANAP_printQ(spr8_lookup_s[f_tile], spr8_lookup_e[f_tile], f_lin, s_col1);

  // DOWN RIGHT
  f_lin = f_lin + 8;
  f_index = f_index + 31;
  f_tile = scr_map[f_index];
  // spr_draw8(scr_map[f_index = +31], f_lin, s_col0);
  NIRVANAP_printQ(spr8_lookup_s[f_tile], spr8_lookup_e[f_tile], f_lin, s_col0);

  // DOWN LEFT
  // spr_draw8(scr_map[f_index++], f_lin, s_col1);
  f_index++;
  f_tile = scr_map[f_index];
  NIRVANAP_printQ(spr8_lookup_s[f_tile], spr8_lookup_e[f_tile], f_lin, s_col1);

  // DOWN RIGHT
  f_lin = f_lin + 8;
  f_index = f_index + 31;
  f_tile = scr_map[f_index];
  // spr_draw8(scr_map[f_index = +31], f_lin, s_col0);
  NIRVANAP_printQ(spr8_lookup_s[f_tile], spr8_lookup_e[f_tile], f_lin, s_col0);

  // DOWN LEFT
  // spr_draw8(scr_map[f_index++], f_lin, s_col1);
  f_index++;
  f_tile = scr_map[f_index];
  NIRVANAP_printQ(spr8_lookup_s[f_tile], spr8_lookup_e[f_tile], f_lin, s_col1);
}

void spr_fill_lookup_table() {
  v0 = 0;
  while (v0 < 32) {

    gbyte = &btiles[0] + (48 * (v0 >> 2));
    switch (v0 & 3) {
    case 0:
    //zx_print_int(23,0,(unsigned int) gbyte);
      spr8_lookup_s[v0] = (unsigned int) gbyte; //TODO z88dk bug? por que el cast
      spr8_lookup_e[v0] = gbyte + 32;
      break;
    case 1:
      spr8_lookup_s[v0] = gbyte + 1;
      spr8_lookup_e[v0] = gbyte + 40;
      break;
    case 2:
      spr8_lookup_s[v0] = gbyte + 16;
      spr8_lookup_e[v0] = gbyte + 36;
      break;
    case 3:
      spr8_lookup_s[v0] = gbyte + 17;
      spr8_lookup_e[v0] = gbyte + 44;
      break;
    }

    ++v0;
  }
}
