/*
        This file is part of NamCap.

        NamCap is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        NamCap is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with NamCap.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "game.h"
#include "game_audio.h"
#include "game_banks.h"
#include "game_enemies.h"
#include "game_engine.h"
#include "game_menu.h"
#include "game_player.h"
#include "game_sprite.h"
#include "game_zx.h"
#include "macros.h"
#include <arch/zx.h>
#include <arch/zx/nirvana+.h>
#include <intrinsic.h>
#include <stdlib.h>
#include <string.h>
#include <z80.h>

void game_start_timer(void) {
  // Hook Timer Function to Nirvana Interrupts
  NIRVANAP_ISR_HOOK[0] = 205;                                // call
  z80_wpoke(&NIRVANAP_ISR_HOOK[1], (unsigned int)game_tick); // game_tick
}

void game_tick(void) {
  ++curr_time;

  zx_isr();
}

/* Main Game Loop  */
void game_loop(void) {

  curr_time = 0;

  player_lives = 3;
  player_score = 0;
  game_over = 0;
  game_round_up = 0;
  // scr_curr = 0xFF;
  map_paper = PAPER_BLACK;


  game_round_init();
  NIRVANAP_halt();
  if (!game_atrac) audio_beep_fx(1);

  /* Game Loop */
  while (!game_over) {
    while (!game_round_up && !game_over) {
      /* Enemies turn */
      enemy_turn();
      /* Player1 turn */
      player_turn();

      if (game_check_time(&time_event, TIME_EVENT)) {
#ifdef DEBUG
        game_fps();
#endif
        time_event = zx_clock();
        // if ( (rand() & 10) == 0 )
        game_add_fruit();
      }
#ifdef DEBUG
      ++fps;
#endif
      ++loop_count;
      if (BIT_CHK(state[INDEX_P1], STAT_KILLED)) {
        // Player Killer
        player_lost_life();
      }
    }

    if (game_round_up) {
      zx_print_ink(INK_BLACK | PAPER_YELLOW);
      ++scr_curr; //Overflow on 256!
      game_round_init();
      game_sprite_restart(0);
      game_sprite_restart(1);
      game_sprite_restart(2);
      game_sprite_restart(3);
      NIRVANAP_spriteT(NIRV_SPRITE_P1, 0, 0, 0);
      NIRVANAP_spriteT(NIRV_SPRITE_FRUIT, 0, 0, 0);
      spr_kind[INDEX_FRUIT] = E_NONE;
      last_time[INDEX_FRUIT] = zx_clock();
      direction[INDEX_P1] = DIR_RIGHT;
      lin[INDEX_P1] = value_d[INDEX_P1];
      col[INDEX_P1] = value_e[INDEX_P1];
      NIRVANAP_halt();

      game_round_up = 0;
    }
  }
}

void game_sprite_restart(unsigned char f_sprite) {
  NIRVANAP_spriteT(f_sprite, 0, 0, 0);

  s_lin0 = lin[f_sprite];
  s_col0 = col[f_sprite];
  index0 = s_col0 + ((s_lin0 >> 3) << 5);
  spr_back_repaint();
  lin[f_sprite] = value_d[f_sprite];
  col[f_sprite] = value_e[f_sprite];
  colint[f_sprite] = 0;

  spr_kind[f_sprite] = E_HORIZONTAL;
  spr_frames[f_sprite] = 4;
  spr_lin_inc[f_sprite] = 2;
  game_scr_val();
  spr_speed[f_sprite] = scr_speed[v0];
  value_a[f_sprite] = value_c[f_sprite];
  last_time[f_sprite] = 0xFFFF; // zx_clock() - (rand() & 4) * 500;

  if ((rand() & 1)) {
    direction[f_sprite] = DIR_RIGHT;
  } else {
    direction[f_sprite] = DIR_LEFT;
  }
  enemy_custom(f_sprite);
}



#ifdef DEBUG
void game_fps(void) {
  zx_print_ink(INK_WHITE);
  zx_print_int(23, 24, fps);
  fps = 0;
}
#endif

void game_draw_map(void) {
  unsigned char val0;
  // Map colors
  map_paper = PAPER_BLACK; // btiles[32] & 0xF8;
  map_clear = map_paper | (map_paper >> 3);
  spr_count = 0;
  obj_count = 0;

  index1 = 32; // Fake 2 rows for nirvana
  s_col1 = 0;
  s_lin1 = 8;
  // Game variables for loop reusage
  game_object_count = 0;

  while (index1 <
         game_max_index - 32) { // Skip last row as it will have normal attribs
    val0 = scr_map[index1];
    if (val0 < 32) {
      // Regular tile
      //spr_draw8(val0, s_lin1, s_col1);
      NIRVANAP_printQ(spr8_lookup_s[val0], spr8_lookup_e[val0], s_lin1, s_col1);

      if (tile_class[scr_map[index1]] == TILE_DOT) {
        ++game_object_count;
      }
    } else {
      /* Clear Map */
      if (val0 == 127) {

        /* Player Init */
        player_lin_scr = s_lin1;
        player_col_scr = s_col1;
        value_a[INDEX_P1] = scr_map[index1 + 1];  // Facing Right/Left
        value_b[INDEX_P1] = scr_map[index1 + 32]; // Initial Colint
      }

      if (val0 == 126) {
        /* Enemy Init */
        enemy_init(64);
        enemy_init(66);
        enemy_init(68);
        enemy_init(70);
      }

      scr_map[index1] = TILE_EMPTY;
      scr_map[index1 + 1] = TILE_EMPTY;
      scr_map[index1 + 32] = TILE_EMPTY;
      scr_map[index1 + 33] = TILE_EMPTY;

      NIRVANAP_printQ(spr8_lookup_s[TILE_EMPTY], spr8_lookup_e[TILE_EMPTY], s_lin1, s_col1);
    }

    ++index1;
    ++s_col1;
    if (s_col1 >= SCR_COLS) {
      s_col1 = 0;
      s_lin1 = s_lin1 + 8;
    }
  }
}

void game_round_init(void) {
  // TODO move to 128 bank's
  unsigned char i;
  unsigned char game_borders;

  // TODO Just use start to simplify
  ay_reset();

  // All Black
  spr_clear_scr();

  NIRVANAP_halt();
  NIRVANAP_stop();
  intrinsic_di();
  game_scr_val();
  scr_blue_time = 50 * scr_blue[v0];
  scr_blink_time = 50 * scr_blink[v0];

  // Get Border Data
  game_borders = INK_BLACK;

  map_border = game_borders;
  /* screen init */
  /*PHASE INIT*/
  loop_count = 0;

  zx_set_clock(0);
  last_time[INDEX_FRUIT] = zx_clock();
  time_event = 0;
  player_coins = 0;

  // Coin HIGHLIGHT init
  key_last = 0;
  key_ink = INK_YELLOW;
  i = 0;
  while (i < MAX_OBJECTS) {
    obj_col[i] = 0xFF;
    obj_lin[i] = 0xFF;
    ++i;
  }

  key_attrib[0] = map_paper | key_ink;
  key_attrib[1] = key_attrib[0];
  key_attrib[2] = key_attrib[0];
  key_attrib[3] = key_attrib[0];

  // Round presentation
  // Draw Screen
  game_page_map();
  game_draw_map();

  if (!game_over && !game_atrac) { // For Attract modes
    // Init Player
    player_init(player_lin_scr, player_col_scr, TILE_P1_RIGHT);
    //Draw Player
    NIRVANAP_spriteT(NIRV_SPRITE_P1, tile[INDEX_P1] + colint[INDEX_P1],
                     lin[INDEX_P1], col[INDEX_P1]);
    BIT_CLR(state[INDEX_P1], STAT_KILLED);
    // Start Tune
    audio_ingame();
  }
  zx_border(map_border);
  game_print_header();
#ifdef DEBUG
  fps = 0;
  zx_print_ink(INK_WHITE | PAPER_BLACK);
  zx_print_chr(23, 20, scr_curr);
#endif
  intrinsic_ei();
  NIRVANAP_start();
  game_print_footer();
}

void game_print_header() {
  // Header

  zx_print_ink(INK_BLUE | (map_border << 3) | BRIGHT);
  game_fill_row(0, 94);
  zx_print_str(0, 0, "{");
  zx_print_str(0, 31, "}");
  zx_print_str(0, 15, "()");

  zx_print_ink(INK_RED | (map_border << 3) | BRIGHT);

  zx_print_str(0, 2, "SCORE");
  zx_print_str(0, 18, "HIGH");
  zx_print_ink(INK_WHITE | (map_border << 3) | BRIGHT);
  zx_print_str(0, 13, "0");
  zx_print_str(0, 29, "0");

  game_print_score();
}

void game_print_footer() {
  // Footer
  zx_print_ink(INK_BLUE | (map_border << 3) | BRIGHT);
  game_fill_row(23, 94);
  zx_print_str(23, 0, "[");
  zx_print_str(23, 31, "]");

  if (!game_atrac)
    game_print_lives();
}

void game_print_score(void) {

  zx_print_ink(INK_WHITE | BRIGHT);
  zx_print_paper(PAPER_BLACK);
  zx_print_int(0, 8, player_score);
  zx_print_int(0, 24, game_score_top); // SCORE TOP
}

void game_print_lives(void) {
  unsigned char f_col = 3;
  zx_print_ink(INK_YELLOW);
  if (player_lives <= 10) {
    v0 = 0;
    while (v0 < (player_lives - 1)) {
      zx_print_str(23, f_col + v0, "<");
      ++v0;
    }
  } else {
    zx_print_str(23, f_col, "<x");
    zx_print_chr(23, f_col + 2, player_lives);
  }
}


void game_cell_paint_index() {

  s_col1 = index1 % 32;
  s_lin1 = (index1 / 32) * 8;
  s_row1 = (s_lin1 >> 3) + 1;
  // s_lin1 = s_lin1 + 16;
  // game_cell_paint();
  spr_draw8(scr_map[index1], s_row1 << 3, s_col1);
}

void game_cell_paint() { spr_draw8(scr_map[index1], s_row1 << 3, s_col1); }

void game_end() {}

void game_cls() {
  NIRVANAP_halt();
  spr_clear_scr();
  zx_paper_fill(INK_BLACK | PAPER_BLACK);
  zx_border(INK_BLACK);
}

void game_paint_attrib(unsigned char *f_attrib[], char f_start,
                       unsigned char f_end, unsigned char f_lin) {

  unsigned char li;
  for (li = f_start; li < f_end; ++li) {
    NIRVANAP_paintC(f_attrib, f_lin, li);
  }
}

unsigned char game_check_time(unsigned int *start, unsigned int lapse) {
  if (zx_clock() - *start > lapse) {
    return 1;
  } else {
    return 0;
  }
}

void game_key_paint(void) {
  unsigned char *p_col;
  unsigned char *p_lin;
  // TODO SPEEDUP ASM ROUTINE
  v0 = 0;
  p_col = &obj_col[v0];
  p_lin = &obj_lin[v0];
  while (v0 < MAX_OBJECTS) {

    if (*p_col != 0xFF) {
      v1 = map_paper | key_ink;
      key_attrib[0] = v1;
      key_attrib[1] = v1;
      key_attrib[2] = v1;
      key_attrib[3] = v1;
      NIRVANAP_paintC(key_attrib, *p_lin, *p_col);
      // game_set_attr(*p_lin, *p_col, v1);
      // game_set_attr(*p_lin + 2, *p_col, v1);
      // game_set_attr(*p_lin + 4, *p_col, v1);
      // game_set_attr(*p_lin + 6, *p_col, v1);
    }
    ++v0;
    ++p_col;
    ++p_lin;
    ++key_ink;
    if (key_ink > 6) {
      key_ink = 3;
    }
  }
}

void game_attribs() {

  // ATTRIB NORMAL
  game_attrib(INK_YELLOW);

  // ATTRIB HIGHLIGHT
  attrib_1[0] = map_paper | INK_CYAN | PAPER_RED;
  attrib_1[1] = map_paper | INK_CYAN | PAPER_RED | BRIGHT;
  attrib_1[2] = map_paper | INK_WHITE | PAPER_RED;
  attrib_1[3] = map_paper | INK_CYAN | PAPER_RED;
}

void game_attrib( unsigned char f_color ) {
  // ATTRIB NORMAL
  attrib[0] = map_paper | f_color;
  attrib[1] = map_paper | f_color | BRIGHT;
  attrib[2] = map_paper | INK_WHITE;
  attrib[3] = map_paper | f_color;
}

void game_page_map(void) {

  unsigned int i0 = 0; // NIRVANA START AT -8PX, SO WE WILL FAKE 2 ROWS
  unsigned int i1 = 0;
  // Unpack Maps
  while (i0 < game_max_index) {
    v0 = world0[i1];
    v1 = world0[i1 + 1];
    if (v0 < 128) {
      // Tile
      scr_map[i0] = v0;
      ++i0;
      ++i1;
    } else {
      // Repeated Tile
      v2 = v0 - 128;
      v0 = 0;
      while (v0 < v2) {
        scr_map[i0] = v1;
        ++v0;
        ++i0;
      }
      ++i1;
      ++i1;
    }
  }
}

void page(unsigned char bank) {
  GLOBAL_ZX_PORT_7FFD = 0x10 + bank;
  IO_7FFD = 0x10 + bank;
}

void zx_print_char(unsigned char ui_row, unsigned char ui_col,
                   unsigned char c) {
  unsigned char str[2];
  str[0] = c;
  str[1] = 0;
  zx_print_str(ui_row, ui_col, str);
}

void game_pause0() {
  v0 = 0;
  while (v0 == 0) {
    v0 = in_test_key();
  }
  in_wait_nokey();
}

void game_add_fruit() {

  if (spr_kind[INDEX_FRUIT] == E_NONE) {
    if (game_check_time(&last_time[INDEX_FRUIT], TIME_FRUIT)) {
     //if ( rand() <= 96 ) {
     if ( (rand()&3) == 0 ) {
       if (!game_atrac) audio_fx(7);
       if (!game_atrac) audio_fx2(128);

        game_scr_val();


        NIRVANAP_spriteT(NIRV_SPRITE_FRUIT, scr_fruit[v0], value_d[INDEX_P1],
                         value_e[INDEX_P1]);
        lin[INDEX_FRUIT] = value_d[INDEX_P1];
        col[INDEX_FRUIT] = value_e[INDEX_P1];
        colint[INDEX_FRUIT] = 0;
        spr_kind[INDEX_FRUIT] = E_FRUIT;
        ++spr_count;
     }
    }
  }
}

void game_sprite_reset() {
  /* code */
  map_paper = PAPER_BLACK;
  spr_count = 0;
  sprite = 0;
  while (sprite < GAME_MAX_ENEMIES) {
    NIRVANAP_spriteT(sprite,0,0,0);
    spr_kind[sprite] = E_NONE;
    col[sprite] = 0;
    lin[sprite] = 0;
    colint[sprite] = 0;
    spr_tile[sprite] = 0;
    tile[sprite] = 0;
    value_a[sprite] = 0;
    value_b[sprite] = 0;
    value_c[sprite] = 0;
    value_d[sprite] = 0;
    value_e[sprite] = 0;
    last_lin[sprite] = 0xFF;
    last_col[sprite] = 0xFF;
    ++sprite;
  }
  index0 = 0;
  while (index0 < game_max_index) {
    scr_map[index0] = 0;
    ++index0;
  }
  spr_kind[INDEX_P1] = E_NONE;


}

void game_scr_val(){
  if (scr_curr > GAME_MAX_VALUES) {
    v0 = 18;
  } else {
    v0 = scr_curr;
  }
}
