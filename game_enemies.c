/*
        This file is part of NamCap.

        NamCap is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        NamCap is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with NamCap.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "game.h"
#include "game_audio.h"
#include "game_banks.h"
#include "game_enemies.h"
#include "game_engine.h"
#include "game_player.h"
#include "game_sprite.h"
#include "game_zx.h"
#include "macros.h"
#include <arch/zx.h>
#include <arch/zx/nirvana+.h>
#include <input.h>
#include <stdlib.h>

void enemy_turn(void) {

  sprite = 0;
  while (sprite < 4) {
    if (spr_chktime()) {
      s_lin0 = lin[sprite];
      s_col0 = col[sprite];
      s_colint0 = colint[sprite];
      index0 = s_col0 + ((s_lin0 >> 3) << 5); // spr_calc_index(s_lin0, s_col0);
      switch (spr_kind[sprite]) {
      case E_HORIZONTAL:

        enemy_move_horizontal();
        enemy_move();
        enemy_draw();
        /* Release a Ghost */
        if ((col[sprite] == value_b[sprite]) && ((rand() & 10) == 0) &&
            !(game_atrac == 1)) {
          direction[sprite] = DIR_UP;
          spr_kind[sprite] = E_GHOST;
          lin[sprite] = lin[sprite] - 2; // Forzar Salida
          NIRVANAP_spriteT(sprite, 0, 0, 0);
          NIRVANAP_halt();
          // NIRVANAP_drawT( PAPER_BLACK | INK_BLACK, lin[sprite], col[sprite]);
          NIRVANAP_fillT(PAPER_BLACK | INK_BLACK, s_lin0, s_col0);
        }
        break;
      case E_GHOST:
        enemy_move_ghost();
        enemy_move();
        enemy_draw();
        break;
      case E_WIMP:
        enemy_move_ghost();
        enemy_move();
        enemy_draw();
        if (game_check_time(&last_time_a[sprite], scr_blue_time)) {
          spr_kind[sprite] = E_WIMP_ALT;
          last_time_a[sprite] = zx_clock();
          value_a[sprite] = 64;
        }
        break;
      case E_WIMP_ALT:
        enemy_move_ghost();
        enemy_move();
        enemy_draw();
        if (game_check_time(&last_time_a[sprite], scr_blink_time)) {
          value_a[sprite] = value_c[sprite];
          spr_kind[sprite] = E_GHOST;
        }
        break;
      case E_EYES:

        enemy_move_ghost();
        enemy_move();
        enemy_draw();

        // Smart Down
        if (lin[sprite] == 56 && (col[sprite] == 9 || col[sprite] == 21)) {
          direction[sprite] = DIR_DOWN;
          lin[sprite] = lin[sprite] + 8;
        }

        // Smart UP
        if (lin[sprite] == 152 && (col[sprite] == 9 || col[sprite] == 21)) {
          direction[sprite] = DIR_UP;
          lin[sprite] = lin[sprite] - 8;
        }

        if (lin[sprite] == 104 && col[sprite] == 15) {
          s_lin0 = lin[sprite];
          s_col0 = col[sprite];
          index0 =
              s_col0 + ((s_lin0 >> 3) << 5); // spr_calc_index(s_lin0, s_col0);
          NIRVANAP_spriteT(sprite, 0, 0, 0);

          spr_back_repaint();
          lin[sprite] = 104 + 8;
          direction[sprite] = DIR_DOWN;
        }

        if (lin[sprite] == value_d[sprite] && col[sprite] == value_e[sprite]) {
          // FIX DOOR
          NIRVANAP_spriteT(sprite, 0, 0, 0);
          NIRVANAP_halt();
          spr_back_repaint();
          spr_frames[sprite] = 4;
          spr_lin_inc[sprite] = 2;
          spr_kind[sprite] = E_HORIZONTAL;
          game_scr_val();
          spr_speed[sprite] = scr_speed[v0];

          value_a[sprite] = value_c[sprite];
          last_time[sprite] = 0xFFFF;
          if ((rand() & 1)) {
            direction[sprite] = DIR_RIGHT;
          } else {
            direction[sprite] = DIR_LEFT;
          }
        }

        break;
      }
    }
    ++sprite;
  }
}

void enemy_draw() {
  // Draw Sprite (Move The sprite in Nirvana, for next frame)
  spr_paint();
  // Repaint Background
  // Can't draw over a Nirvana sprite, so we need to move it first
  // Smart Repaint Backgound

  spr_back_repaint_dir();
}

void enemy_move_horizontal(void) {

  if (((lin[sprite] & 7) == 0) && (colint[sprite] == 0)) {
    switch (direction[sprite]) {
    case DIR_LEFT:
      if (player_check_left()) {
        // spr_move_left();
      } else {
        direction[sprite] = DIR_RIGHT;
      }
      break;
    case DIR_RIGHT:
      if (player_check_right()) {
        // spr_move_right();
      } else {
        direction[sprite] = DIR_LEFT;
      }
      break;
    }
  }
}

void enemy_move_ghost(void) {

  if ((s_lin0 & 7) == 0) {
    if (direction[sprite] == DIR_UP || direction[sprite] == DIR_DOWN) {
      // Vertical
      if (direction[sprite] == DIR_UP) {
        if (!player_check_up()) {
          enemy_change_dir_fh();
        } else {
          enemy_change_dir_h();
        }
      } else {
        if (!player_check_down()) {
          enemy_change_dir_fh();
        } else {
          enemy_change_dir_h();
        }
      }
    } else {
      // Horizontal
      if (direction[sprite] == DIR_RIGHT) {
        if (!player_check_right()) {
          enemy_change_dir_fv();
        } else {
          if (s_colint0 == 0) {
            enemy_change_dir_v();
          }
        }
      } else {
        if (!player_check_left()) {
          enemy_change_dir_fv();
        } else {
          if (s_colint0 == 3) {
            enemy_change_dir_v();
          }
        }
      }
    }
  }
}

void enemy_change_dir_v() {
  enemy_brain_v();
  if (player_check_down() && v0) {
    spr_set_down();
  } else {
    if (player_check_up() && !v0) {
      spr_set_up();
    }
  }
}

void enemy_change_dir_h() {
  enemy_brain_h();
  if (player_check_right() && v0) {
    spr_set_right();
  } else {
    if (player_check_left() && !v0) {
      spr_set_left();
    }
  }
}

void enemy_change_dir_fh() {
  enemy_brain_h();
  if (v0) {
    if (player_check_right()) {
      spr_set_right();
    } else {
      spr_set_left();
    }
  } else {
    if (player_check_left()) {
      spr_set_left();
    } else {
      spr_set_right();
    }
  }
}

void enemy_change_dir_fv() {
  enemy_brain_v();
  if (v0) {
    if (player_check_down()) {
      spr_set_down();
    } else {
      spr_set_up();
    }
  } else {
    if (player_check_up()) {
      spr_set_up();
    } else {
      spr_set_down();
    }
  }
}

void enemy_brain_h() {

  // GAME LOGIC
  v0 = 0;
  switch (spr_kind[sprite]) {
  case E_EYES:
    if (s_col0 <= 15) {
      v0 = 1;
    }
    break;

  case E_WIMP:
    v0 = rand() & 1;
    if (s_col0 > col[INDEX_P1]) { // Arranca de P1
      v0 = 1;
    }
    break;

  case E_WIMP_ALT :
    v0 = rand() & 1;
    if (s_col0 > col[INDEX_P1]) { // Arranca de P1
      v0 = 1;
    }
    break;

  case E_GHOST:
    switch (sprite) {
    case 0: // Red ghost, Blinky, doggedly pursues Pac-Man;
      if (s_col0 < col[INDEX_P1]) {
        v0 = 1;
      }
      break;
    case 1: // Pink ghost, Pinky, tries to ambush Pac-Man by moving parallel to
      // him;
      if (s_col0 < col[INDEX_P1]) {
        v0 = 1;
      }
      break;
    case 2: // Cyan ghost, Inky, tends not to chase Pac-Man directly unless
            // Blinky is near;
      if ((abs(lin[0] - lin[2]) < 64) && (abs(col[0] - col[2]) < 16)) {
        if (s_col0 < col[INDEX_P1]) {
          v0 = 1;
        }
      } else {
        v0 = rand() & 1;
      }
      break;
    case 3: // Orange ghost, Clyde, pursues Pac-Man when far from him, but
            // usually wanders away when he gets close.
      if ((abs(lin[0] - lin[2]) > 64) && (abs(col[0] - col[2]) > 16)) {
        if (s_col0 < col[INDEX_P1]) {
          v0 = 1;
        }
      } else {
        v0 = rand() & 1;
      }
      break;
    }
    break;
  }
}

void enemy_brain_v() {

  // GAME LOGIC
  v0 = 0;
  switch (spr_kind[sprite]) {
  case E_EYES:
    if (s_lin0 <= 128) {
      v0 = 1;
    }
    break;

  case E_WIMP:
    v0 = rand() & 1;
    if (s_lin0 > lin[INDEX_P1]) { // Arranca de P1
      v0 = 1;
    }
    break;

  case E_WIMP_ALT :
    v0 = rand() & 1;
    if (s_lin0 > lin[INDEX_P1]) { // Arranca de P1
      v0 = 1;
    }
    break;

  case E_GHOST:
    switch (sprite) {
    case 0: // Red ghost, Blinky, doggedly pursues Pac-Man;
      if (s_lin0 < lin[INDEX_P1]) {
        v0 = 1;
      }
      break;
    case 1: // Pink ghost, Pinky, tries to ambush Pac-Man by moving parallel to
            // him;
      v0 = rand() & 1;
      break;
    case 2: // Cyan ghost, Inky, tends not to chase Pac-Man directly unless
            // Blinky is near;
      if ((abs(lin[0] - lin[2]) < 64) && (abs(col[0] - col[2]) < 16)) {
        if (s_lin0 < lin[INDEX_P1]) {
          v0 = 1;
        }
      } else {
        v0 = rand() & 1;
      }
      break;
    case 3: // Orange ghost, Clyde, pursues Pac-Man when far from him, but
            // usually wanders away when he gets close.
      if ((abs(lin[0] - lin[2]) > 64) && (abs(col[0] - col[2]) > 16)) {
        if (s_lin0 < lin[INDEX_P1]) {
          v0 = 1;
        }
      } else {
        v0 = rand() & 1;
      }
      break;
    }
  }
}

void enemy_move() {
  switch (direction[sprite]) {
  case DIR_UP:
    tile[sprite] = value_a[sprite] + 4;
    spr_move_up();
    break;
  case DIR_DOWN:
    tile[sprite] = value_a[sprite] + 4;
    spr_move_down();
    break;
  case DIR_LEFT:
    tile[sprite] = value_a[sprite];
    spr_move_left();
    break;
  case DIR_RIGHT:
    tile[sprite] = value_a[sprite];
    spr_move_right();
    break;
  };
}

void enemy_init(unsigned char f_class) {

  unsigned char f_tot_class;
  unsigned char f_basetile;
  unsigned int f_pos;

  unsigned char *f_init;

  // Found an empty sprite slot
  f_tot_class = 0;
  sprite = 0;
  while (sprite < GAME_MAX_ENEMIES) {
    if (spr_kind[sprite] == E_NONE) {
      // found a record
      break;
    }
    ++sprite;
  }

  f_init = &spr_init[0];

  // Search for Sprite Attribs on spr_init
  f_pos = 0;
  // Size of spr_init attributes
  while (f_pos <= (GAME_TOTAL_INDEX_CLASSES * GAME_SPR_INIT_SIZE)) {

    if (*(f_init + f_pos) == f_class) {

      // Read from ARRAY
      f_basetile = *(f_init + f_pos + 1);         // base tile
      spr_frames[sprite] = *(f_init + f_pos + 2); // frames
      spr_altset[sprite] = *(f_init + f_pos + 3);
      spr_kind[sprite] = *(f_init + f_pos + 4); // Class of sprite
      spr_lin_inc[sprite] = 2;
      // Read From Map
      class[sprite] = f_class;
      game_scr_val();
      spr_speed[sprite] = scr_speed[v0];
      // spr_speed[sprite] = scr_map[index1 + 1];
      // value_a[sprite] = scr_map[index1 + 32];
      // value_b[sprite] = scr_map[index1 + 33];

      // Class Found!
      state[sprite] = 0;
      if (rand() & 1) {
        direction[sprite] = DIR_RIGHT;
      } else {
        direction[sprite] = DIR_LEFT;
      }

      // Position
      lin[sprite] = (index1 / SCR_COLS) << 3; //*8
      col[sprite] = index1 & 31;

      colint[sprite] = 0;
      spr_tile[sprite] = f_basetile;
      tile[sprite] = f_basetile;
      value_a[sprite] = f_basetile;
      value_b[sprite] = col[sprite];
      value_c[sprite] = f_basetile;
      value_d[sprite] = lin[sprite];
      value_e[sprite] = col[sprite];
      last_lin[sprite] = 0xFF;
      last_col[sprite] = 0xFF;
      enemy_custom(sprite);

      ++spr_count;
      last_time[sprite] = 0xFFFF; // zx_clock() - (rand() & 4) * 500;

      break;
    } else {
      // Next Class increment
      f_pos = f_pos + GAME_SPR_INIT_SIZE; // N variables on spr_init
    }
  }
}

void enemy_custom(unsigned char f_sprite) {
  switch (f_sprite) {
  case 0:
    lin[f_sprite] = lin[f_sprite] - 24;
    spr_kind[f_sprite] = E_GHOST;
    break;

  case 2:
    col[f_sprite] = col[f_sprite] - 2;
    break;
  case 3:
    col[f_sprite] = col[f_sprite] + 2;
    break;
  }
}

void enemy_kill(unsigned char f_sprite) {

  NIRVANAP_spriteT(f_sprite, 0, 0, 0);
  NIRVANAP_spriteT(NIRV_SPRITE_P1, 0, 0, 0);
  NIRVANAP_fillT(PAPER_BLACK || INK_BLACK, lin[f_sprite], col[f_sprite]);
  NIRVANAP_fillT(PAPER_BLACK || INK_BLACK, lin[INDEX_P1], col[INDEX_P1]);

  NIRVANAP_halt();
  NIRVANAP_printQ(spr8_lookup_s[4 + player_eat_count],
                  spr8_lookup_e[4 + player_eat_count], lin[f_sprite],
                  col[f_sprite]);
  NIRVANAP_printQ(spr8_lookup_s[8], spr8_lookup_e[8], lin[f_sprite],
                  col[f_sprite] + 1);

  if (!game_atrac) {
    audio_eat_ghost();
  } else {
    z80_delay_ms(125);
  }
  lin[f_sprite] = (lin[f_sprite] >> 3) << 3;
  colint[f_sprite] = 0;
  spr_kind[f_sprite] = E_EYES;
  value_a[f_sprite] = 72;
  spr_frames[f_sprite] = 1;
  spr_lin_inc[f_sprite] = 8;
  spr_speed[f_sprite] = ENEMY_FAST_SPEED;
  last_time[f_sprite] = 0xFFFF;

  v0 = 20 << player_eat_count;
  if (!game_atrac)
    player_score_add(v0);
  ++player_eat_count;
}
